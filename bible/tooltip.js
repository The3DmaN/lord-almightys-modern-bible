(function(Tooltip) {
	if (window.Tooltip) {
		console.error(new Error("Conflict! window.Tooltip already exists!"));
	} else {
		Tooltip();
	}
})(function() {
	function Tooltip(target, message, options) {
		this.target = target;
		this.message = message;
		this.options = options;
		this.tooltip = null;
		this.arrow = null;
		this.width = null;
	};
	Tooltip.api = Tooltip.prototype = {
		_: {
			setup: function(Tooltip) {
				if (Tooltip.options) {
					for (var option in Tooltip.options) {
						if (Tooltip.hasOwnProperty(option)) {
							Tooltip[option] = Tooltip.options[option];
						} else if (window.Tooltip.api.hasOwnProperty(option)) {
							window.Tooltip.api[option].call(Tooltip, Tooltip.options[option]);
						};
					};
				};
			},
			make: {
				tooltip: function(Tooltip) {
					Tooltip.tooltip.classList.add("tooltip-tip");
					Tooltip.tooltip.appendChild(this.arrow(Tooltip));
					Tooltip.tooltip.appendChild(this.message(Tooltip));
						// get tooltip width:
						Tooltip.tooltip.style.visibility = "hidden";
						document.body.appendChild(Tooltip.tooltip);
						Tooltip.width = Tooltip.tooltip.offsetWidth;
						document.body.removeChild(Tooltip.tooltip);
						Tooltip.tooltip.style.visibility = "visible";
					return Tooltip.tooltip;
				},
				arrow: function(Tooltip) {
					Tooltip.arrow = document.createElement("div");
					Tooltip.arrow.classList.add("tooltip-arrow");
					return Tooltip.arrow;
				},
				message: function(Tooltip) {
					var message = document.createElement("div");
					message.classList.add("tooltip-message");
					if (Tooltip.message) message.textContent = Tooltip.message;
					if (Tooltip.options && Tooltip.options.html) message.innerHTML = Tooltip.options.html;
					return message;
				}
			}
		},
		observe: function() {
			var _this = this;
			if (!(_this.target instanceof HTMLElement) && typeof _this.target === "object") {
				_this.options = _this.target;
			} else if (typeof _this.message === "object") {
				_this.options = _this.message;
			};
			_this.tooltip = document.createElement("div");
			_this._.setup(_this);
			_this._.make.tooltip(_this);
			_this.target.addEventListener(_this.options && _this.options.event || "click", function(e) {
				var target = _this.target;
				if ((target.offsetLeft + target.offsetWidth/2) - (_this.width/2) < 0) {
					_this.tooltip.style.left = target.offsetLeft + "px";
				} else {
					_this.tooltip.style.left = target.offsetLeft + (target.offsetWidth/2) - (_this.width/2) + "px";
					_this.arrow.style.left = (_this.width/2) - 8 + "px";
				};
				_this.tooltip.style.top = _this.target.offsetTop + _this.target.offsetHeight + 8 + "px";
				document.body.appendChild(_this.tooltip);
			});
			_this.target.addEventListener("mouseleave", function() {
				if (_this.tooltip.parentNode) {
					document.body.addEventListener("click", function myClick() {
						_this.tooltip.parentNode.removeChild(_this.tooltip);
						document.body.removeEventListener('click', myClick, false);
					});
				};
			});
		}
	};
	window.Tooltip = Tooltip;
	window.addEventListener("load", function() {
		var tooltips = document.querySelectorAll("[data-tooltip]");
		for (var i = 0, count = tooltips.length; i < count; i ++) {
			new Tooltip(tooltips[i], tooltips[i].dataset.tooltip).observe() || "missing message argument...";
		};
	});
});
document.addEventListener('contextmenu', event => event.preventDefault());
