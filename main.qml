/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1


ApplicationWindow {
    id: window
    width: mainsettings.appwidth
    height: mainsettings.appheight
    visible: true
    title: qsTr(" ")
    flags: {
        if (screen.width < 800) {
            Qt.FramelessWindowHint
        }
    }
    Settings {
        id:mainsettings
        property string newhomepage: "Home.qml"
        property int appwidth: 360
        property int appheight: 720
        property string themesetting: "Light"
        property string textsize: "Medium"
        property string booktitle: ""
        property string bookurl: ""
        property string resetbooks: "NO"
    }
    onClosing: {
        mainsettings.appwidth = window.width
        mainsettings.appheight = window.height
    }

    Material.theme: {
        if (mainsettings.themesetting === "OLED") {
            Material.primary="#212121"
            Material.foreground="antiquewhite"
            Material.background="Black"
            Material.accent="antiquewhite"

        } else if (mainsettings.themesetting === "Light"){
            Material.primary="antiquewhite"
            Material.foreground="dimgrey"
            Material.background="antiquewhite"
            Material.accent="dimgrey"

        } else if (mainsettings.themesetting === "Dark"){
            Material.primary="dimgrey"
            Material.foreground="antiquewhite"
            Material.background="dimgrey"
            Material.accent="antiquewhite"

        }

    }
    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        Material.theme: {
                if (mainsettings.themesetting === "OLED") {
                    Material.primary="#212121"
                    Material.foreground="antiquewhite"
                    Material.background="Black"
                    Material.accent="antiquewhite"

                } else if (mainsettings.themesetting === "Light"){
                    Material.primary="antiquewhite"
                    Material.foreground="dimgrey"
                    Material.background="antiquewhite"
                    Material.accent="dimgrey"

                } else if (mainsettings.themesetting === "Dark"){
                    Material.primary="dimgrey"
                    Material.foreground="antiquewhite"
                    Material.background="dimgrey"
                    Material.accent="antiquewhite"

                }

            }

        ToolButton {
            id: toolButton
            text: "<font>\u2630</font>"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: drawer.open()
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }
    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height
        ScrollView {
            width: parent.width
            height : parent.height
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn
            contentWidth: column.width
            contentHeight: column.height
            Column {
                width: parent.width
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                Label {
                    text: qsTr("Lord Almighty's Modern Bible")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                ItemDelegate {
                    text: qsTr("Cover")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="Home.qml"
                        stackView.replace("Home.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Preface")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Preface"
                        mainsettings.bookurl="qrc:files/bible/FRT.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Glossary")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Glossary"
                        mainsettings.bookurl="qrc:files/bible/GLO.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Settings")
                    width: parent.width
                    onClicked: {
                        stackView.replace("SettingsForm.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("About")
                    width: parent.width
                    onClicked: {
                        stackView.replace("About.qml")
                        drawer.close()
                    }
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                Label {
                    text: qsTr("Old Testament Books")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                ItemDelegate {
                    text: qsTr("Genesis")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Genesis"
                        mainsettings.bookurl="qrc:files/bible/GEN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Exodus")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Exodus"
                        mainsettings.bookurl="qrc:files/bible/EXO.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Leviticus")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Leviticus"
                        mainsettings.bookurl="qrc:files/bible/LEV.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Numbers")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Numbers"
                        mainsettings.bookurl="qrc:files/bible/NUM.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Deuteronomy")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Deuteronomy"
                        mainsettings.bookurl="qrc:files/bible/DEU.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Joshua")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Joshua"
                        mainsettings.bookurl="qrc:files/bible/JOS.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Judges")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Judges"
                        mainsettings.bookurl="qrc:files/bible/JDG.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ruth")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Ruth"
                        mainsettings.bookurl="qrc:files/bible/RUT.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Samuel")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Samuel"
                        mainsettings.bookurl="qrc:files/bible/1SA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Samuel")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Samuel"
                        mainsettings.bookurl="qrc:files/bible/2SA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Kings")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Kings"
                        mainsettings.bookurl="qrc:files/bible/1KI.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Kings")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Kings"
                        mainsettings.bookurl="qrc:files/bible/2KI.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Chronicles")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Chronicles"
                        mainsettings.bookurl="qrc:files/bible/1CH.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Chronicles")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Chronicles"
                        mainsettings.bookurl="qrc:files/bible/2CH.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ezra")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Ezra"
                        mainsettings.bookurl="qrc:files/bible/EZR.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Nehemiah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Nehemiah"
                        mainsettings.bookurl="qrc:files/bible/NEH.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Esther")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Esther"
                        mainsettings.bookurl="qrc:files/bible/EST.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Job")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Job"
                        mainsettings.bookurl="qrc:files/bible/JOB.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Psalms")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Psalms"
                        mainsettings.bookurl="qrc:files/bible/PSA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Proverbs")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Proverbs"
                        mainsettings.bookurl="qrc:files/bible/PRO.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ecclesiastes")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Ecclesiastes"
                        mainsettings.bookurl="qrc:files/bible/ECC.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Song of Solomon")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Song of Solomon"
                        mainsettings.bookurl="qrc:files/bible/SNG.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Isaiah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Isaiah"
                        mainsettings.bookurl="qrc:files/bible/ISA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Jeremiah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Jeremiah"
                        mainsettings.bookurl="qrc:files/bible/JER.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Lamentations")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Lamentations"
                        mainsettings.bookurl="qrc:files/bible/LAM.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ezekiel")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Ezekiel"
                        mainsettings.bookurl="qrc:files/bible/EZK.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Daniel")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Daniel"
                        mainsettings.bookurl="qrc:files/bible/DAN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Hosea")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Hosea"
                        mainsettings.bookurl="qrc:files/bible/HOS.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Joel")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Joel"
                        mainsettings.bookurl="qrc:files/bible/JOL.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Amos")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Amos"
                        mainsettings.bookurl="qrc:files/bible/AMO.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Obadiah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Obadiah"
                        mainsettings.bookurl="qrc:files/bible/OBA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Jonah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Jonah"
                        mainsettings.bookurl="qrc:files/bible/JON.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Micah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Micah"
                        mainsettings.bookurl="qrc:files/bible/MIC.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Nahum")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Nahum"
                        mainsettings.bookurl="qrc:files/bible/NAM.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Habakkuk")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Habakkuk"
                        mainsettings.bookurl="qrc:files/bible/HAB.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Zephaniah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Zephaniah"
                        mainsettings.bookurl="qrc:files/bible/ZEP.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Haggai")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Haggai"
                        mainsettings.bookurl="qrc:files/bible/HAG.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Zechariah")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Zechariah"
                        mainsettings.bookurl="qrc:files/bible/ZEC.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Malachi")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Malachi"
                        mainsettings.bookurl="qrc:files/bible/MAL.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }  
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                Label {
                    text: qsTr("New Testament Books")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                ItemDelegate {
                    text: qsTr("Matthew")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Matthew"
                        mainsettings.bookurl="qrc:files/bible/MAT.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Mark")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Mark"
                        mainsettings.bookurl="qrc:files/bible/MRK.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Luke")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Luke"
                        mainsettings.bookurl="qrc:files/bible/LUK.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("John")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="John"
                        mainsettings.bookurl="qrc:files/bible/JHN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Acts")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Acts"
                        mainsettings.bookurl="qrc:files/bible/ACT.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Romans")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Romans"
                        mainsettings.bookurl="qrc:files/bible/ROM.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Corinthians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Corinthians"
                        mainsettings.bookurl="qrc:files/bible/1CO.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Corinthians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Corinthians"
                        mainsettings.bookurl="qrc:files/bible/2CO.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Galatians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Galatians"
                        mainsettings.bookurl="qrc:files/bible/GAL.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ephesians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Ephesians"
                        mainsettings.bookurl="qrc:files/bible/EPH.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Philippians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Philippians"
                        mainsettings.bookurl="qrc:files/bible/PHP.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Colossians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Colossians"
                        mainsettings.bookurl="qrc:files/bible/COL.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Thessalonians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Thessalonians"
                        mainsettings.bookurl="qrc:files/bible/1TH.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Thessalonians")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Thessalonians"
                        mainsettings.bookurl="qrc:files/bible/2TH.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Timothy")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Timothy"
                        mainsettings.bookurl="qrc:files/bible/1TI.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Timothy")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Timothy"
                        mainsettings.bookurl="qrc:files/bible/2TI.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Titus")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Titus"
                        mainsettings.bookurl="qrc:files/bible/TIT.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Philemon")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Philemon"
                        mainsettings.bookurl="qrc:files/bible/PHM.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Hebrews")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Hebrews"
                        mainsettings.bookurl="qrc:files/bible/HEB.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("James")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="James"
                        mainsettings.bookurl="qrc:files/bible/JAS.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Peter")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Peter"
                        mainsettings.bookurl="qrc:files/bible/1PE.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Peter")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Peter"
                        mainsettings.bookurl="qrc:files/bible/2PE.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 John")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 John"
                        mainsettings.bookurl="qrc:files/bible/1JN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 John")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 John"
                        mainsettings.bookurl="qrc:files/bible/2JN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("3 John")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="3 John"
                        mainsettings.bookurl="qrc:files/bible/3JN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Jude")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Jude"
                        mainsettings.bookurl="qrc:files/bible/JUD.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Revelation")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Revelation"
                        mainsettings.bookurl="qrc:files/bible/REV.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                Label {
                    text: qsTr("Deuterocanon Books")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                    }
                }
                ItemDelegate {
                    text: qsTr("Tobit")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Tobit"
                        mainsettings.bookurl="qrc:files/bible/TOB.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Judith")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Judith"
                        mainsettings.bookurl="qrc:files/bible/JDT.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Esther (Greek)")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Esther (Greek)"
                        mainsettings.bookurl="qrc:files/bible/ESG.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Wisdom of Solomon")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Wisdom of Solomon"
                        mainsettings.bookurl="qrc:files/bible/WIS.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Sirach")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Sirach"
                        mainsettings.bookurl="qrc:files/bible/SIR.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Baruch")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Baruch"
                        mainsettings.bookurl="qrc:files/bible/BAR.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Maccabees")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Maccabees"
                        mainsettings.bookurl="qrc:files/bible/1MA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Maccabees")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Maccabees"
                        mainsettings.bookurl="qrc:files/bible/2MA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Esdras")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="1 Esdras"
                        mainsettings.bookurl="qrc:files/bible/1ES.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Prayer of Manasses")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Prayer of Manasses"
                        mainsettings.bookurl="qrc:files/bible/MAN.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Psalm 151")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Psalm 151"
                        mainsettings.bookurl="qrc:files/bible/PS2.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("3 Maccabees")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="3 Maccabees"
                        mainsettings.bookurl="qrc:files/bible/3MA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Esdras")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="2 Esdras"
                        mainsettings.bookurl="qrc:files/bible/2ES.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("4 Maccabees")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="4 Maccabees"
                        mainsettings.bookurl="qrc:files/bible/4MA.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Daniel (Greek)")
                    width: parent.width
                    onClicked: {
                        mainsettings.newhomepage="BookPage.qml"
                        mainsettings.booktitle="Daniel (Greek)"
                        mainsettings.bookurl="qrc:files/bible/DAG.xhtml"
                        stackView.replace("BookPage.qml")
                        drawer.close()
                    }
                }
            }
        }
    }
    StackView {
        id: stackView

        initialItem: {
                stackView.replace(mainsettings.newhomepage)
        }
        anchors.fill: parent
    }
}
