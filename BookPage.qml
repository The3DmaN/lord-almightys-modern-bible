/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookgen
    title: mainsettings.booktitle
    property string pagepos: {
        if (mainsettings.booktitle === "Glossary") {
            "window.scrollTo(0," + settings.restorescrollglo + ");"
        } else if (mainsettings.booktitle === "Preface") {
            "window.scrollTo(0," + settings.restorescrollfrt + ");"
        } else if (mainsettings.booktitle === "Genesis") {
            "window.scrollTo(0," + settings.restorescroll + ");"
        } else if (mainsettings.booktitle === "Exodus") {
            "window.scrollTo(0," + settings.restorescrollexo + ");"
        } else if (mainsettings.booktitle === "Leviticus") {
            "window.scrollTo(0," + settings.restorescrolllev + ");"
        } else if (mainsettings.booktitle === "Numbers") {
            "window.scrollTo(0," + settings.restorescrollnum + ");"
        } else if (mainsettings.booktitle === "Deuteronomy") {
            "window.scrollTo(0," + settings.restorescrolldeu + ");"
        } else if (mainsettings.booktitle === "Joshua") {
            "window.scrollTo(0," + settings.restorescrolljos + ");"
        } else if (mainsettings.booktitle === "Judges") {
            "window.scrollTo(0," + settings.restorescrolljdg + ");"
        } else if (mainsettings.booktitle === "Ruth") {
            "window.scrollTo(0," + settings.restorescrollrut + ");"
        } else if (mainsettings.booktitle === "1 Samuel") {
            "window.scrollTo(0," + settings.restorescroll1sa + ");"
        } else if (mainsettings.booktitle === "2 Samuel") {
            "window.scrollTo(0," + settings.restorescroll2sa + ");"
        } else if (mainsettings.booktitle === "1 Kings") {
            "window.scrollTo(0," + settings.restorescroll1ki + ");"
        } else if (mainsettings.booktitle === "2 Kings") {
            "window.scrollTo(0," + settings.restorescroll2ki + ");"
        } else if (mainsettings.booktitle === "1 Chronicles") {
            "window.scrollTo(0," + settings.restorescroll1ch + ");"
        } else if (mainsettings.booktitle === "2 Chronicles") {
            "window.scrollTo(0," + settings.restorescroll2ch + ");"
        } else if (mainsettings.booktitle === "Ezra") {
            "window.scrollTo(0," + settings.restorescrollezr + ");"
        } else if (mainsettings.booktitle === "Nehemiah") {
            "window.scrollTo(0," + settings.restorescrollneh + ");"
        } else if (mainsettings.booktitle === "Esther") {
            "window.scrollTo(0," + settings.restorescrollest + ");"
        } else if (mainsettings.booktitle === "Job") {
            "window.scrollTo(0," + settings.restorescrolljob + ");"
        } else if (mainsettings.booktitle === "Psalms") {
            "window.scrollTo(0," + settings.restorescrollpsa + ");"
        } else if (mainsettings.booktitle === "Proverbs") {
            "window.scrollTo(0," + settings.restorescrollpro + ");"
        } else if (mainsettings.booktitle === "Ecclesiastes") {
            "window.scrollTo(0," + settings.restorescrollecc + ");"
        } else if (mainsettings.booktitle === "Song of Solomon") {
            "window.scrollTo(0," + settings.restorescrollsng + ");"
        } else if (mainsettings.booktitle === "Isaiah") {
            "window.scrollTo(0," + settings.restorescrollisa + ");"
        } else if (mainsettings.booktitle === "Jeremiah") {
            "window.scrollTo(0," + settings.restorescrolljer + ");"
        } else if (mainsettings.booktitle === "Lamentations") {
            "window.scrollTo(0," + settings.restorescrolllam + ");"
        } else if (mainsettings.booktitle === "Ezekiel") {
            "window.scrollTo(0," + settings.restorescrollezk + ");"
        } else if (mainsettings.booktitle === "Daniel") {
            "window.scrollTo(0," + settings.restorescrolldan + ");"
        } else if (mainsettings.booktitle === "Hosea") {
            "window.scrollTo(0," + settings.restorescrollhos + ");"
        } else if (mainsettings.booktitle === "Joel") {
            "window.scrollTo(0," + settings.restorescrolljol + ");"
        } else if (mainsettings.booktitle === "Amos") {
            "window.scrollTo(0," + settings.restorescrollamo + ");"
        } else if (mainsettings.booktitle === "Obadiah") {
            "window.scrollTo(0," + settings.restorescrolloba + ");"
        } else if (mainsettings.booktitle === "Jonah") {
            "window.scrollTo(0," + settings.restorescrolljon + ");"
        } else if (mainsettings.booktitle === "Micah") {
            "window.scrollTo(0," + settings.restorescrollmic + ");"
        } else if (mainsettings.booktitle === "Nahum") {
            "window.scrollTo(0," + settings.restorescrollnam + ");"
        } else if (mainsettings.booktitle === "Habakkuk") {
            "window.scrollTo(0," + settings.restorescrollhab + ");"
        } else if (mainsettings.booktitle === "Zephaniah") {
            "window.scrollTo(0," + settings.restorescrollzep + ");"
        } else if (mainsettings.booktitle === "Haggai") {
            "window.scrollTo(0," + settings.restorescrollhag + ");"
        } else if (mainsettings.booktitle === "Zechariah") {
            "window.scrollTo(0," + settings.restorescrollzec + ");"
        } else if (mainsettings.booktitle === "Malachi") {
            "window.scrollTo(0," + settings.restorescrollmal + ");"
        } else if (mainsettings.booktitle === "Matthew") {
            "window.scrollTo(0," + settings.restorescrollmat + ");"
        } else if (mainsettings.booktitle === "Mark") {
            "window.scrollTo(0," + settings.restorescrollmrk + ");"
        } else if (mainsettings.booktitle === "Luke") {
            "window.scrollTo(0," + settings.restorescrollluk + ");"
        } else if (mainsettings.booktitle === "John") {
            "window.scrollTo(0," + settings.restorescrolljhn + ");"
        } else if (mainsettings.booktitle === "Acts") {
            "window.scrollTo(0," + settings.restorescrollact + ");"
        } else if (mainsettings.booktitle === "Romans") {
            "window.scrollTo(0," + settings.restorescrollrom + ");"
        } else if (mainsettings.booktitle === "1 Corinthians") {
            "window.scrollTo(0," + settings.restorescroll1co + ");"
        } else if (mainsettings.booktitle === "2 Corinthians") {
            "window.scrollTo(0," + settings.restorescroll2co + ");"
        } else if (mainsettings.booktitle === "Galatians") {
            "window.scrollTo(0," + settings.restorescrollgal + ");"
        } else if (mainsettings.booktitle === "Ephesians") {
            "window.scrollTo(0," + settings.restorescrolleph + ");"
        } else if (mainsettings.booktitle === "Philippians") {
            "window.scrollTo(0," + settings.restorescrollphp + ");"
        } else if (mainsettings.booktitle === "Colossians") {
            "window.scrollTo(0," + settings.restorescrollcol + ");"
        } else if (mainsettings.booktitle === "1 Thessalonians") {
            "window.scrollTo(0," + settings.restorescroll1th + ");"
        } else if (mainsettings.booktitle === "2 Thessalonians") {
            "window.scrollTo(0," + settings.restorescroll2th + ");"
        } else if (mainsettings.booktitle === "1 Timothy") {
            "window.scrollTo(0," + settings.restorescroll1ti + ");"
        } else if (mainsettings.booktitle === "2 Timothy") {
            "window.scrollTo(0," + settings.restorescroll2ti + ");"
        } else if (mainsettings.booktitle === "Titus") {
            "window.scrollTo(0," + settings.restorescrolltit + ");"
        } else if (mainsettings.booktitle === "Philemon") {
            "window.scrollTo(0," + settings.restorescrollphm + ");"
        } else if (mainsettings.booktitle === "Hebrews") {
            "window.scrollTo(0," + settings.restorescrollheb + ");"
        } else if (mainsettings.booktitle === "James") {
            "window.scrollTo(0," + settings.restorescrolljas + ");"
        } else if (mainsettings.booktitle === "1 Peter") {
            "window.scrollTo(0," + settings.restorescroll1pe + ");"
        } else if (mainsettings.booktitle === "2 Peter") {
            "window.scrollTo(0," + settings.restorescroll2pe + ");"
        } else if (mainsettings.booktitle === "1 John") {
            "window.scrollTo(0," + settings.restorescroll1jn + ");"
        } else if (mainsettings.booktitle === "2 John") {
            "window.scrollTo(0," + settings.restorescroll2jn + ");"
        } else if (mainsettings.booktitle === "3 John") {
            "window.scrollTo(0," + settings.restorescroll3jn + ");"
        } else if (mainsettings.booktitle === "Jude") {
            "window.scrollTo(0," + settings.restorescrolljud + ");"
        } else if (mainsettings.booktitle === "Revelation") {
            "window.scrollTo(0," + settings.restorescrollrev + ");"
        } else if (mainsettings.booktitle === "Tobit") {
            "window.scrollTo(0," + settings.restorescrolltob + ");"
        } else if (mainsettings.booktitle === "Judith") {
            "window.scrollTo(0," + settings.restorescrolljdt + ");"
        } else if (mainsettings.booktitle === "Esther (Greek)") {
            "window.scrollTo(0," + settings.restorescrollesg + ");"
        } else if (mainsettings.booktitle === "Wisdom of Solomon") {
            "window.scrollTo(0," + settings.restorescrollwis + ");"
        } else if (mainsettings.booktitle === "Sirach") {
            "window.scrollTo(0," + settings.restorescrollsir + ");"
        } else if (mainsettings.booktitle === "Baruch") {
            "window.scrollTo(0," + settings.restorescrollbar + ");"
        } else if (mainsettings.booktitle === "1 Maccabees") {
            "window.scrollTo(0," + settings.restorescroll1ma + ");"
        } else if (mainsettings.booktitle === "2 Maccabees") {
            "window.scrollTo(0," + settings.restorescroll2ma + ");"
        } else if (mainsettings.booktitle === "1 Esdras") {
            "window.scrollTo(0," + settings.restorescroll1es + ");"
        } else if (mainsettings.booktitle === "Prayer of Manasses") {
            "window.scrollTo(0," + settings.restorescrollman + ");"
        } else if (mainsettings.booktitle === "Psalm 151") {
            "window.scrollTo(0," + settings.restorescrollps2 + ");"
        } else if (mainsettings.booktitle === "3 Maccabees") {
            "window.scrollTo(0," + settings.restorescroll3ma + ");"
        } else if (mainsettings.booktitle === "2 Esdras") {
            "window.scrollTo(0," + settings.restorescroll2es + ");"
        } else if (mainsettings.booktitle === "4 Maccabees") {
            "window.scrollTo(0," + settings.restorescroll4ma + ");"
        } else if (mainsettings.booktitle === "Daniel (Greek)") {
            "window.scrollTo(0," + settings.restorescrolldag + ");"
        }
    }

    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
            if (mainsettings.themesetting === "OLED") {
                Material.primary="#212121"
                Material.foreground="antiquewhite"
                Material.background="Black"
                Material.accent="antiquewhite"

            } else if (mainsettings.themesetting === "Light"){
                Material.primary="antiquewhite"
                Material.foreground="dimgrey"
                Material.background="antiquewhite"
                Material.accent="dimgrey"

            } else if (mainsettings.themesetting === "Dark"){
                Material.primary="dimgrey"
                Material.foreground="antiquewhite"
                Material.background="dimgrey"
                Material.accent="antiquewhite"

            }

        }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ComboBox {
                id: control
                model: {
                    if (mainsettings.booktitle === "Glossary") {
                        glomod
                    } else if (mainsettings.booktitle === "Preface") {
                        frtmod
                    } else if (mainsettings.booktitle === "Genesis") {
                        genmod
                    } else if (mainsettings.booktitle === "Exodus") {
                        exomod
                    } else if (mainsettings.booktitle === "Leviticus") {
                        levmod
                    } else if (mainsettings.booktitle === "Numbers") {
                        nummod
                    } else if (mainsettings.booktitle === "Deuteronomy") {
                        deumod
                    } else if (mainsettings.booktitle === "Joshua") {
                        josmod
                    } else if (mainsettings.booktitle === "Judges") {
                        jdgmod
                    } else if (mainsettings.booktitle === "Ruth") {
                        rutmod
                    } else if (mainsettings.booktitle === "1 Samuel") {
                        onesamod
                    } else if (mainsettings.booktitle === "2 Samuel") {
                        twosamod
                    } else if (mainsettings.booktitle === "1 Kings") {
                        onekimod
                    } else if (mainsettings.booktitle === "2 Kings") {
                        twokimod
                    } else if (mainsettings.booktitle === "1 Chronicles") {
                        onechmod
                    } else if (mainsettings.booktitle === "2 Chronicles") {
                        twochmod
                    } else if (mainsettings.booktitle === "Ezra") {
                        ezrmod
                    } else if (mainsettings.booktitle === "Nehemiah") {
                        nehmod
                    } else if (mainsettings.booktitle === "Esther") {
                        estmod
                    } else if (mainsettings.booktitle === "Job") {
                        jobmod
                    } else if (mainsettings.booktitle === "Psalms") {
                        psamod
                    } else if (mainsettings.booktitle === "Proverbs") {
                        promod
                    } else if (mainsettings.booktitle === "Ecclesiastes") {
                        eccmod
                    } else if (mainsettings.booktitle === "Song of Solomon") {
                        sngmod
                    } else if (mainsettings.booktitle === "Isaiah") {
                        isamod
                    } else if (mainsettings.booktitle === "Jeremiah") {
                        jermod
                    } else if (mainsettings.booktitle === "Lamentations") {
                        lammod
                    } else if (mainsettings.booktitle === "Ezekiel") {
                        ezkmod
                    } else if (mainsettings.booktitle === "Hosea") {
                        hosmod
                    } else if (mainsettings.booktitle === "Joel") {
                        jolmod
                    } else if (mainsettings.booktitle === "Amos") {
                        amomod
                    } else if (mainsettings.booktitle === "Obadiah") {
                        obamod
                    } else if (mainsettings.booktitle === "Jonah") {
                        jonmod
                    } else if (mainsettings.booktitle === "Micah") {
                        micmod
                    } else if (mainsettings.booktitle === "Nahum") {
                        nammod
                    } else if (mainsettings.booktitle === "Habakkuk") {
                        habmod
                    } else if (mainsettings.booktitle === "Zephaniah") {
                        zepmod
                    } else if (mainsettings.booktitle === "Haggai") {
                        hagmod
                    } else if (mainsettings.booktitle === "Zechariah") {
                        zecmod
                    } else if (mainsettings.booktitle === "Malachi") {
                        malmod
                    } else if (mainsettings.booktitle === "Matthew") {
                        matmod
                    } else if (mainsettings.booktitle === "Mark") {
                        mrkmod
                    } else if (mainsettings.booktitle === "Luke") {
                        lukmod
                    } else if (mainsettings.booktitle === "John") {
                        jhnmod
                    } else if (mainsettings.booktitle === "Acts") {
                        actmod
                    } else if (mainsettings.booktitle === "Romans") {
                        rommod
                    } else if (mainsettings.booktitle === "1 Corinthians") {
                        onecomod
                    } else if (mainsettings.booktitle === "2 Corinthians") {
                        twocomod
                    } else if (mainsettings.booktitle === "Galatians") {
                        galmod
                    } else if (mainsettings.booktitle === "Ephesians") {
                        ephmod
                    } else if (mainsettings.booktitle === "Philippians") {
                        phpmod
                    } else if (mainsettings.booktitle === "Colossians") {
                        colmod
                    } else if (mainsettings.booktitle === "1 Thessalonians") {
                        onethmod
                    } else if (mainsettings.booktitle === "2 Thessalonians") {
                        twothmod
                    } else if (mainsettings.booktitle === "1 Timothy") {
                        onetimod
                    } else if (mainsettings.booktitle === "2 Timothy") {
                        twotimod
                    } else if (mainsettings.booktitle === "Titus") {
                        titmod
                    } else if (mainsettings.booktitle === "Philemon") {
                        phmmod
                    } else if (mainsettings.booktitle === "Hebrews") {
                        hebmod
                    } else if (mainsettings.booktitle === "James") {
                        jasmod
                    } else if (mainsettings.booktitle === "1 Peter") {
                        onepemod
                    } else if (mainsettings.booktitle === "2 Peter") {
                        twopemod
                    } else if (mainsettings.booktitle === "1 John") {
                        onejnmod
                    } else if (mainsettings.booktitle === "2 John") {
                        twojnmod
                    } else if (mainsettings.booktitle === "3 John") {
                        threejnmod
                    } else if (mainsettings.booktitle === "Jude") {
                        judmod
                    } else if (mainsettings.booktitle === "Revelation") {
                        revmod
                    } else if (mainsettings.booktitle === "Tobit") {
                        tobmod
                    } else if (mainsettings.booktitle === "Judith") {
                        jdtmod
                    } else if (mainsettings.booktitle === "Esther (Greek)") {
                        esgmod
                    } else if (mainsettings.booktitle === "Wisdom of Solomon") {
                        wismod
                    } else if (mainsettings.booktitle === "Sirach") {
                        sirmod
                    } else if (mainsettings.booktitle === "Baruch") {
                        barmod
                    } else if (mainsettings.booktitle === "1 Maccabees") {
                        onemamod
                    } else if (mainsettings.booktitle === "2 Maccabees") {
                        twomamod
                    } else if (mainsettings.booktitle === "1 Esdras") {
                        oneesmod
                    } else if (mainsettings.booktitle === "Prayer of Manasses") {
                        manmod
                    } else if (mainsettings.booktitle === "Psalm 151") {
                        ps2mod
                    } else if (mainsettings.booktitle === "3 Maccabees") {
                        threemamod
                    } else if (mainsettings.booktitle === "2 Esdras") {
                        twoesmod
                    } else if (mainsettings.booktitle === "4 Maccabees") {
                        fourmamod
                    } else if (mainsettings.booktitle === "Daniel (Greek)") {
                        dagmod
                    }
                }

                textRole: "text"
                displayText: "Select Chapter "
                width: 250
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(mainsettings.themesetting === "Dark" || mainsettings.themesetting === "OLED"){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(mainsettings.themesetting === "Dark" || mainsettings.themesetting === "OLED"){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav = true
                    settings.bookloc = "#" + model.get(index).value
                    settings.finalurl = mainsettings.bookurl + settings.bookloc
                    webview.url = settings.finalurl
                }
            }
        }
    }
    Settings {
        id: settings
        property real restorescrollglo: 0
        property real restorescrollfrt: 0
        property real restorescroll: 0
        property real restorescrollexo: 0
        property real restorescrolllev: 0
        property real restorescrollnum: 0
        property real restorescrolldeu: 0
        property real restorescrolljos: 0
        property real restorescrolljdg: 0
        property real restorescrollrut: 0
        property real restorescroll1sa: 0
        property real restorescroll2sa: 0
        property real restorescroll1ki: 0
        property real restorescroll2ki: 0
        property real restorescroll1ch: 0
        property real restorescroll2ch: 0
        property real restorescrollezr: 0
        property real restorescrollneh: 0
        property real restorescrollest: 0
        property real restorescrolljob: 0
        property real restorescrollpsa: 0
        property real restorescrollpro: 0
        property real restorescrollecc: 0
        property real restorescrollsng: 0
        property real restorescrollisa: 0
        property real restorescrolljer: 0
        property real restorescrolllam: 0
        property real restorescrollezk: 0
        property real restorescrolldan: 0
        property real restorescrollhos: 0
        property real restorescrolljol: 0
        property real restorescrollamo: 0
        property real restorescrolloba: 0
        property real restorescrolljon: 0
        property real restorescrollmic: 0
        property real restorescrollnam: 0
        property real restorescrollhab: 0
        property real restorescrollzep: 0
        property real restorescrollhag: 0
        property real restorescrollzec: 0
        property real restorescrollmal: 0
        property real restorescrollmat: 0
        property real restorescrollmrk: 0
        property real restorescrollluk: 0
        property real restorescrolljhn: 0
        property real restorescrollact: 0
        property real restorescrollrom: 0
        property real restorescroll1co: 0
        property real restorescroll2co: 0
        property real restorescrollgal: 0
        property real restorescrolleph: 0
        property real restorescrollphp: 0
        property real restorescrollcol: 0
        property real restorescroll1th: 0
        property real restorescroll2th: 0
        property real restorescroll1ti: 0
        property real restorescroll2ti: 0
        property real restorescrolltit: 0
        property real restorescrollphm: 0
        property real restorescrollheb: 0
        property real restorescrolljas: 0
        property real restorescroll1pe: 0
        property real restorescroll2pe: 0
        property real restorescroll1jn: 0
        property real restorescroll2jn: 0
        property real restorescroll3jn: 0
        property real restorescrolljud: 0
        property real restorescrollrev: 0
        property real restorescrolltob: 0
        property real restorescrolljdt: 0
        property real restorescrollesg: 0
        property real restorescrollwis: 0
        property real restorescrollsir: 0
        property real restorescrollbar: 0
        property real restorescroll1ma: 0
        property real restorescroll2ma: 0
        property real restorescroll1es: 0
        property real restorescrollman: 0
        property real restorescrollps2: 0
        property real restorescroll3ma: 0
        property real restorescroll2es: 0
        property real restorescroll4ma: 0
        property real restorescrolldag: 0
        property string bookloc: ""
        property string finalurl: ""
        property bool isusernav: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: mainsettings.bookurl
        onScrollPositionChanged: {
            if (mainsettings.booktitle === "Glossary") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollglo = (result);})
            } else if (mainsettings.booktitle === "Preface") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollfrt = (result);})
            } else if (mainsettings.booktitle === "Genesis") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll = (result);})
            } else if (mainsettings.booktitle === "Exodus") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollexo = (result);})
            } else if (mainsettings.booktitle === "Leviticus") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolllev = (result);})
            } else if (mainsettings.booktitle === "Numbers") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollnum = (result);})
            } else if (mainsettings.booktitle === "Deuteronomy") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolldeu = (result);})
            } else if (mainsettings.booktitle === "Joshua") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljos = (result);})
            } else if (mainsettings.booktitle === "Judges") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljdg = (result);})
            } else if (mainsettings.booktitle === "Ruth") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollrut = (result);})
            } else if (mainsettings.booktitle === "1 Samuel") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1sa = (result);})
            } else if (mainsettings.booktitle === "2 Samuel") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2sa = (result);})
            } else if (mainsettings.booktitle === "1 Kings") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1ki = (result);})
            } else if (mainsettings.booktitle === "2 Kings") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2ki = (result);})
            } else if (mainsettings.booktitle === "1 Chronicles") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1ch = (result);})
            } else if (mainsettings.booktitle === "2 Chronicles") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2ch = (result);})
            } else if (mainsettings.booktitle === "Ezra") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollezr = (result);})
            } else if (mainsettings.booktitle === "Nehemiah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollneh = (result);})
            } else if (mainsettings.booktitle === "Esther") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollest = (result);})
            } else if (mainsettings.booktitle === "Job") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljob = (result);})
            } else if (mainsettings.booktitle === "Psalms") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollpsa = (result);})
            } else if (mainsettings.booktitle === "Proverbs") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollpro = (result);})
            } else if (mainsettings.booktitle === "Ecclesiastes") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollecc = (result);})
            } else if (mainsettings.booktitle === "Song of Solomon") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollsng = (result);})
            } else if (mainsettings.booktitle === "Isaiah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollisa = (result);})
            } else if (mainsettings.booktitle === "Jeremiah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljer = (result);})
            } else if (mainsettings.booktitle === "Lamentations") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolllam = (result);})
            } else if (mainsettings.booktitle === "Ezekiel") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollezk = (result);})
            } else if (mainsettings.booktitle === "Daniel") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolldan = (result);})
            } else if (mainsettings.booktitle === "Hosea") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollhos = (result);})
            } else if (mainsettings.booktitle === "Joel") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljol = (result);})
            } else if (mainsettings.booktitle === "Amos") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollamo = (result);})
            } else if (mainsettings.booktitle === "Obadiah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolloba = (result);})
            } else if (mainsettings.booktitle === "Jonah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljon = (result);})
            } else if (mainsettings.booktitle === "Micah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollmic = (result);})
            } else if (mainsettings.booktitle === "Nahum") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollnam = (result);})
            } else if (mainsettings.booktitle === "Habakkuk") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollhab = (result);})
            } else if (mainsettings.booktitle === "Zephaniah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollzep = (result);})
            } else if (mainsettings.booktitle === "Haggai") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollhag = (result);})
            } else if (mainsettings.booktitle === "Zechariah") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollzec = (result);})
            } else if (mainsettings.booktitle === "Malachi") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollmal = (result);})
            } else if (mainsettings.booktitle === "Matthew") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollmat = (result);})
            } else if (mainsettings.booktitle === "Mark") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollmrk = (result);})
            } else if (mainsettings.booktitle === "Luke") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollluk = (result);})
            } else if (mainsettings.booktitle === "John") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljhn = (result);})
            } else if (mainsettings.booktitle === "Acts") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollact = (result);})
            } else if (mainsettings.booktitle === "Romans") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollrom = (result);})
            } else if (mainsettings.booktitle === "1 Corinthians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1co = (result);})
            } else if (mainsettings.booktitle === "2 Corinthians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2co = (result);})
            } else if (mainsettings.booktitle === "Galatians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollgal = (result);})
            } else if (mainsettings.booktitle === "Ephesians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolleph = (result);})
            } else if (mainsettings.booktitle === "Philippians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollphp = (result);})
            } else if (mainsettings.booktitle === "Colossians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollcol = (result);})
            } else if (mainsettings.booktitle === "1 Thessalonians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1th = (result);})
            } else if (mainsettings.booktitle === "2 Thessalonians") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2th = (result);})
            } else if (mainsettings.booktitle === "1 Timothy") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1ti = (result);})
            } else if (mainsettings.booktitle === "2 Timothy") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2ti = (result);})
            } else if (mainsettings.booktitle === "Titus") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolltit = (result);})
            } else if (mainsettings.booktitle === "Philemon") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollphm = (result);})
            } else if (mainsettings.booktitle === "Hebrews") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollheb = (result);})
            } else if (mainsettings.booktitle === "James") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljas = (result);})
            } else if (mainsettings.booktitle === "1 Peter") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1pe = (result);})
            } else if (mainsettings.booktitle === "2 Peter") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2pe = (result);})
            } else if (mainsettings.booktitle === "1 John") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1jn = (result);})
            } else if (mainsettings.booktitle === "2 John") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2jn = (result);})
            } else if (mainsettings.booktitle === "3 John") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll3jn = (result);})
            } else if (mainsettings.booktitle === "Jude") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljud = (result);})
            } else if (mainsettings.booktitle === "Revelation") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollrev = (result);})
            } else if (mainsettings.booktitle === "Tobit") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolltob = (result);})
            } else if (mainsettings.booktitle === "Judith") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljdt = (result);})
            } else if (mainsettings.booktitle === "Esther (Greek)") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollesg = (result);})
            } else if (mainsettings.booktitle === "Wisdom of Solomon") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollwis = (result);})
            } else if (mainsettings.booktitle === "Sirach") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollsir = (result);})
            } else if (mainsettings.booktitle === "Baruch") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollbar = (result);})
            } else if (mainsettings.booktitle === "1 Maccabees") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1ma = (result);})
            } else if (mainsettings.booktitle === "2 Maccabees") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2ma = (result);})
            } else if (mainsettings.booktitle === "1 Esdras") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1es = (result);})
            } else if (mainsettings.booktitle === "Prayer of Manasses") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollman = (result);})
            } else if (mainsettings.booktitle === "Psalm 151") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollps2 = (result);})
            } else if (mainsettings.booktitle === "3 Maccabees") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll3ma = (result);})
            } else if (mainsettings.booktitle === "2 Esdras") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2es = (result);})
            } else if (mainsettings.booktitle === "4 Maccabees") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll4ma = (result);})
            } else if (mainsettings.booktitle === "Daniel (Greek)") {
                runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolldag = (result);})
            }
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            if (mainsettings.textsize === "Small") {
                webview.runJavaScript("document.body.style.zoom= %1".arg(0.8))
            } else if (mainsettings.textsize === "Medium") {
                webview.runJavaScript("document.body.style.zoom= %1".arg(1.0))
            } else if (mainsettings.textsize === "Large"){
                webview.runJavaScript("document.body.style.zoom= %1".arg(1.3))
            }
            if (mainsettings.themesetting === "Dark" || mainsettings.themesetting === "OLED") {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav == false) {
                    if (mainsettings.resetbooks === "YES") {
                        settings.restorescrollglo=0
                        settings.restorescrollfrt=0
                        settings.restorescroll=0
                        settings.restorescrollexo=0
                        settings.restorescrolllev=0
                        settings.restorescrollnum=0
                        settings.restorescrolldeu=0
                        settings.restorescrolljos=0
                        settings.restorescrolljdg=0
                        settings.restorescrollrut=0
                        settings.restorescroll1sa=0
                        settings.restorescroll2sa=0
                        settings.restorescroll1ki=0
                        settings.restorescroll2ki=0
                        settings.restorescroll1ch=0
                        settings.restorescroll2ch=0
                        settings.restorescrollezr=0
                        settings.restorescrollneh=0
                        settings.restorescrollest=0
                        settings.restorescrolljob=0
                        settings.restorescrollpsa=0
                        settings.restorescrollpro=0
                        settings.restorescrollecc=0
                        settings.restorescrollsng=0
                        settings.restorescrollisa=0
                        settings.restorescrolljer=0
                        settings.restorescrolllam=0
                        settings.restorescrollezk=0
                        settings.restorescrolldan=0
                        settings.restorescrollhos=0
                        settings.restorescrolljol=0
                        settings.restorescrollamo=0
                        settings.restorescrolloba=0
                        settings.restorescrolljon=0
                        settings.restorescrollmic=0
                        settings.restorescrollnam=0
                        settings.restorescrollhab=0
                        settings.restorescrollzep=0
                        settings.restorescrollhag=0
                        settings.restorescrollzec=0
                        settings.restorescrollmal=0
                        settings.restorescrollmat=0
                        settings.restorescrollmrk=0
                        settings.restorescrollluk=0
                        settings.restorescrolljhn=0
                        settings.restorescrollact=0
                        settings.restorescrollrom=0
                        settings.restorescroll1co=0
                        settings.restorescroll2co=0
                        settings.restorescrollgal=0
                        settings.restorescrolleph=0
                        settings.restorescrollphp=0
                        settings.restorescrollcol=0
                        settings.restorescroll1th=0
                        settings.restorescroll2th=0
                        settings.restorescroll1ti=0
                        settings.restorescroll2ti=0
                        settings.restorescrolltit=0
                        settings.restorescrollphm=0
                        settings.restorescrollheb=0
                        settings.restorescrolljas=0
                        settings.restorescroll1pe=0
                        settings.restorescroll2pe=0
                        settings.restorescroll1jn=0
                        settings.restorescroll2jn=0
                        settings.restorescroll3jn=0
                        settings.restorescrolljud=0
                        settings.restorescrollrev=0
                        settings.restorescrolltob=0
                        settings.restorescrolljdt=0
                        settings.restorescrollesg=0
                        settings.restorescrollwis=0
                        settings.restorescrollsir=0
                        settings.restorescrollbar=0
                        settings.restorescroll1ma=0
                        settings.restorescroll2ma=0
                        settings.restorescroll1es=0
                        settings.restorescrollman=0
                        settings.restorescrollps2=0
                        settings.restorescroll3ma=0
                        settings.restorescroll2es=0
                        settings.restorescroll4ma=0
                        settings.restorescrolldag=0
                        mainsettings.resetbooks="NO"
                    }
                    runJavaScript(biblebookgen.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: glossmod
        ListElement { text: "Glossary A"; value: "GL1_0";}
        ListElement { text: "Glossary B"; value: "GL2_0";}
        ListElement { text: "Glossary C"; value: "GL3_0";}
        ListElement { text: "Glossary D"; value: "GL4_0";}
        ListElement { text: "Glossary E"; value: "GL5_0";}
        ListElement { text: "Glossary G"; value: "GL7_0";}
        ListElement { text: "Glossary H"; value: "GL8_0";}
        ListElement { text: "Glossary I"; value: "GL9_0";}
        ListElement { text: "Glossary J"; value: "GL10_0";}
        ListElement { text: "Glossary K"; value: "GL11_0";}
        ListElement { text: "Glossary L"; value: "GL12_0";}
        ListElement { text: "Glossary M"; value: "GL13_0";}
        ListElement { text: "Glossary N"; value: "GL14_0";}
        ListElement { text: "Glossary O"; value: "GL15_0";}
        ListElement { text: "Glossary P"; value: "GL16_0";}
        ListElement { text: "Glossary Q"; value: "GL17_0";}
        ListElement { text: "Glossary R"; value: "GL18_0";}
        ListElement { text: "Glossary S"; value: "GL19_0";}
        ListElement { text: "Glossary T"; value: "GL20_0";}
        ListElement { text: "Glossary Y"; value: "GL25_0";}
        ListElement { text: "Glossary Z"; value: "GL26_0";}
    }
    ListModel {
        id: frtmod
        ListElement { text: "Preface 1"; value: "FR1_0";}
    }
    ListModel {
        id: genmod
        ListElement { text: "Genesis 1"; value: "GN1_0";}
        ListElement { text: "Genesis 2"; value: "GN2_0";}
        ListElement { text: "Genesis 3"; value: "GN3_0";}
        ListElement { text: "Genesis 4"; value: "GN4_0";}
        ListElement { text: "Genesis 5"; value: "GN5_0";}
        ListElement { text: "Genesis 6"; value: "GN6_0";}
        ListElement { text: "Genesis 7"; value: "GN7_0";}
        ListElement { text: "Genesis 8"; value: "GN8_0";}
        ListElement { text: "Genesis 9"; value: "GN9_0";}
        ListElement { text: "Genesis 10"; value: "GN10_0";}
        ListElement { text: "Genesis 11"; value: "GN11_0";}
        ListElement { text: "Genesis 12"; value: "GN12_0";}
        ListElement { text: "Genesis 13"; value: "GN13_0";}
        ListElement { text: "Genesis 14"; value: "GN14_0";}
        ListElement { text: "Genesis 15"; value: "GN15_0";}
        ListElement { text: "Genesis 16"; value: "GN16_0";}
        ListElement { text: "Genesis 17"; value: "GN17_0";}
        ListElement { text: "Genesis 18"; value: "GN18_0";}
        ListElement { text: "Genesis 19"; value: "GN19_0";}
        ListElement { text: "Genesis 20"; value: "GN20_0";}
        ListElement { text: "Genesis 21"; value: "GN21_0";}
        ListElement { text: "Genesis 22"; value: "GN22_0";}
        ListElement { text: "Genesis 23"; value: "GN23_0";}
        ListElement { text: "Genesis 24"; value: "GN24_0";}
        ListElement { text: "Genesis 25"; value: "GN25_0";}
        ListElement { text: "Genesis 26"; value: "GN26_0";}
        ListElement { text: "Genesis 27"; value: "GN27_0";}
        ListElement { text: "Genesis 28"; value: "GN28_0";}
        ListElement { text: "Genesis 29"; value: "GN29_0";}
        ListElement { text: "Genesis 30"; value: "GN30_0";}
        ListElement { text: "Genesis 31"; value: "GN31_0";}
        ListElement { text: "Genesis 32"; value: "GN32_0";}
        ListElement { text: "Genesis 33"; value: "GN33_0";}
        ListElement { text: "Genesis 34"; value: "GN34_0";}
        ListElement { text: "Genesis 35"; value: "GN35_0";}
        ListElement { text: "Genesis 36"; value: "GN36_0";}
        ListElement { text: "Genesis 37"; value: "GN37_0";}
        ListElement { text: "Genesis 38"; value: "GN38_0";}
        ListElement { text: "Genesis 39"; value: "GN39_0";}
        ListElement { text: "Genesis 40"; value: "GN40_0";}
        ListElement { text: "Genesis 41"; value: "GN41_0";}
        ListElement { text: "Genesis 42"; value: "GN42_0";}
        ListElement { text: "Genesis 43"; value: "GN43_0";}
        ListElement { text: "Genesis 44"; value: "GN44_0";}
        ListElement { text: "Genesis 45"; value: "GN45_0";}
        ListElement { text: "Genesis 46"; value: "GN46_0";}
        ListElement { text: "Genesis 47"; value: "GN47_0";}
        ListElement { text: "Genesis 48"; value: "GN48_0";}
        ListElement { text: "Genesis 49"; value: "GN49_0";}
        ListElement { text: "Genesis 50"; value: "GN50_0";}
    }
    ListModel {
        id: exomod
        ListElement { text: "Exodus 1"; value: "EX1_0";}
        ListElement { text: "Exodus 2"; value: "EX2_0";}
        ListElement { text: "Exodus 3"; value: "EX3_0";}
        ListElement { text: "Exodus 4"; value: "EX4_0";}
        ListElement { text: "Exodus 5"; value: "EX5_0";}
        ListElement { text: "Exodus 6"; value: "EX6_0";}
        ListElement { text: "Exodus 7"; value: "EX7_0";}
        ListElement { text: "Exodus 8"; value: "EX8_0";}
        ListElement { text: "Exodus 9"; value: "EX9_0";}
        ListElement { text: "Exodus 10"; value: "EX10_0";}
        ListElement { text: "Exodus 11"; value: "EX11_0";}
        ListElement { text: "Exodus 12"; value: "EX12_0";}
        ListElement { text: "Exodus 13"; value: "EX13_0";}
        ListElement { text: "Exodus 14"; value: "EX14_0";}
        ListElement { text: "Exodus 15"; value: "EX15_0";}
        ListElement { text: "Exodus 16"; value: "EX16_0";}
        ListElement { text: "Exodus 17"; value: "EX17_0";}
        ListElement { text: "Exodus 18"; value: "EX18_0";}
        ListElement { text: "Exodus 19"; value: "EX19_0";}
        ListElement { text: "Exodus 20"; value: "EX20_0";}
        ListElement { text: "Exodus 21"; value: "EX21_0";}
        ListElement { text: "Exodus 22"; value: "EX22_0";}
        ListElement { text: "Exodus 23"; value: "EX23_0";}
        ListElement { text: "Exodus 24"; value: "EX24_0";}
        ListElement { text: "Exodus 25"; value: "EX25_0";}
        ListElement { text: "Exodus 26"; value: "EX26_0";}
        ListElement { text: "Exodus 27"; value: "EX27_0";}
        ListElement { text: "Exodus 28"; value: "EX28_0";}
        ListElement { text: "Exodus 29"; value: "EX29_0";}
        ListElement { text: "Exodus 30"; value: "EX30_0";}
        ListElement { text: "Exodus 31"; value: "EX31_0";}
        ListElement { text: "Exodus 32"; value: "EX32_0";}
        ListElement { text: "Exodus 33"; value: "EX33_0";}
        ListElement { text: "Exodus 34"; value: "EX34_0";}
        ListElement { text: "Exodus 35"; value: "EX35_0";}
        ListElement { text: "Exodus 36"; value: "EX36_0";}
        ListElement { text: "Exodus 37"; value: "EX37_0";}
        ListElement { text: "Exodus 38"; value: "EX38_0";}
        ListElement { text: "Exodus 39"; value: "EX39_0";}
        ListElement { text: "Exodus 40"; value: "EX40_0";}
    }
    ListModel {
        id: levmod
        ListElement { text: "Leviticus 1"; value: "LV1_0";}
        ListElement { text: "Leviticus 2"; value: "LV2_0";}
        ListElement { text: "Leviticus 3"; value: "LV3_0";}
        ListElement { text: "Leviticus 4"; value: "LV4_0";}
        ListElement { text: "Leviticus 5"; value: "LV5_0";}
        ListElement { text: "Leviticus 6"; value: "LV6_0";}
        ListElement { text: "Leviticus 7"; value: "LV7_0";}
        ListElement { text: "Leviticus 8"; value: "LV8_0";}
        ListElement { text: "Leviticus 9"; value: "LV9_0";}
        ListElement { text: "Leviticus 10"; value: "LV10_0";}
        ListElement { text: "Leviticus 11"; value: "LV11_0";}
        ListElement { text: "Leviticus 12"; value: "LV12_0";}
        ListElement { text: "Leviticus 13"; value: "LV13_0";}
        ListElement { text: "Leviticus 14"; value: "LV14_0";}
        ListElement { text: "Leviticus 15"; value: "LV15_0";}
        ListElement { text: "Leviticus 16"; value: "LV16_0";}
        ListElement { text: "Leviticus 17"; value: "LV17_0";}
        ListElement { text: "Leviticus 18"; value: "LV18_0";}
        ListElement { text: "Leviticus 19"; value: "LV19_0";}
        ListElement { text: "Leviticus 20"; value: "LV20_0";}
        ListElement { text: "Leviticus 21"; value: "LV21_0";}
        ListElement { text: "Leviticus 22"; value: "LV22_0";}
        ListElement { text: "Leviticus 23"; value: "LV23_0";}
        ListElement { text: "Leviticus 24"; value: "LV24_0";}
        ListElement { text: "Leviticus 25"; value: "LV25_0";}
        ListElement { text: "Leviticus 26"; value: "LV26_0";}
        ListElement { text: "Leviticus 27"; value: "LV27_0";}
    }
    ListModel {
        id: nummod
        ListElement { text: "Numbers 1"; value: "NU1_0";}
        ListElement { text: "Numbers 2"; value: "NU2_0";}
        ListElement { text: "Numbers 3"; value: "NU3_0";}
        ListElement { text: "Numbers 4"; value: "NU4_0";}
        ListElement { text: "Numbers 5"; value: "NU5_0";}
        ListElement { text: "Numbers 6"; value: "NU6_0";}
        ListElement { text: "Numbers 7"; value: "NU7_0";}
        ListElement { text: "Numbers 8"; value: "NU8_0";}
        ListElement { text: "Numbers 9"; value: "NU9_0";}
        ListElement { text: "Numbers 10"; value: "NU10_0";}
        ListElement { text: "Numbers 11"; value: "NU11_0";}
        ListElement { text: "Numbers 12"; value: "NU12_0";}
        ListElement { text: "Numbers 13"; value: "NU13_0";}
        ListElement { text: "Numbers 14"; value: "NU14_0";}
        ListElement { text: "Numbers 15"; value: "NU15_0";}
        ListElement { text: "Numbers 16"; value: "NU16_0";}
        ListElement { text: "Numbers 17"; value: "NU17_0";}
        ListElement { text: "Numbers 18"; value: "NU18_0";}
        ListElement { text: "Numbers 19"; value: "NU19_0";}
        ListElement { text: "Numbers 20"; value: "NU20_0";}
        ListElement { text: "Numbers 21"; value: "NU21_0";}
        ListElement { text: "Numbers 22"; value: "NU22_0";}
        ListElement { text: "Numbers 23"; value: "NU23_0";}
        ListElement { text: "Numbers 24"; value: "NU24_0";}
        ListElement { text: "Numbers 25"; value: "NU25_0";}
        ListElement { text: "Numbers 26"; value: "NU26_0";}
        ListElement { text: "Numbers 27"; value: "NU27_0";}
        ListElement { text: "Numbers 28"; value: "NU28_0";}
        ListElement { text: "Numbers 29"; value: "NU29_0";}
        ListElement { text: "Numbers 30"; value: "NU30_0";}
        ListElement { text: "Numbers 31"; value: "NU31_0";}
        ListElement { text: "Numbers 32"; value: "NU32_0";}
        ListElement { text: "Numbers 33"; value: "NU33_0";}
        ListElement { text: "Numbers 34"; value: "NU34_0";}
        ListElement { text: "Numbers 35"; value: "NU35_0";}
        ListElement { text: "Numbers 36"; value: "NU36_0";}
    }
    ListModel {
        id: deumod
        ListElement { text: "Deuteronomy 1"; value: "DT1_0";}
        ListElement { text: "Deuteronomy 2"; value: "DT2_0";}
        ListElement { text: "Deuteronomy 3"; value: "DT3_0";}
        ListElement { text: "Deuteronomy 4"; value: "DT4_0";}
        ListElement { text: "Deuteronomy 5"; value: "DT5_0";}
        ListElement { text: "Deuteronomy 6"; value: "DT6_0";}
        ListElement { text: "Deuteronomy 7"; value: "DT7_0";}
        ListElement { text: "Deuteronomy 8"; value: "DT8_0";}
        ListElement { text: "Deuteronomy 9"; value: "DT9_0";}
        ListElement { text: "Deuteronomy 10"; value: "DT10_0";}
        ListElement { text: "Deuteronomy 11"; value: "DT11_0";}
        ListElement { text: "Deuteronomy 12"; value: "DT12_0";}
        ListElement { text: "Deuteronomy 13"; value: "DT13_0";}
        ListElement { text: "Deuteronomy 14"; value: "DT14_0";}
        ListElement { text: "Deuteronomy 15"; value: "DT15_0";}
        ListElement { text: "Deuteronomy 16"; value: "DT16_0";}
        ListElement { text: "Deuteronomy 17"; value: "DT17_0";}
        ListElement { text: "Deuteronomy 18"; value: "DT18_0";}
        ListElement { text: "Deuteronomy 19"; value: "DT19_0";}
        ListElement { text: "Deuteronomy 20"; value: "DT20_0";}
        ListElement { text: "Deuteronomy 21"; value: "DT21_0";}
        ListElement { text: "Deuteronomy 22"; value: "DT22_0";}
        ListElement { text: "Deuteronomy 23"; value: "DT23_0";}
        ListElement { text: "Deuteronomy 24"; value: "DT24_0";}
        ListElement { text: "Deuteronomy 25"; value: "DT25_0";}
        ListElement { text: "Deuteronomy 26"; value: "DT26_0";}
        ListElement { text: "Deuteronomy 27"; value: "DT27_0";}
        ListElement { text: "Deuteronomy 28"; value: "DT28_0";}
        ListElement { text: "Deuteronomy 29"; value: "DT29_0";}
        ListElement { text: "Deuteronomy 30"; value: "DT30_0";}
        ListElement { text: "Deuteronomy 31"; value: "DT31_0";}
        ListElement { text: "Deuteronomy 32"; value: "DT32_0";}
        ListElement { text: "Deuteronomy 33"; value: "DT33_0";}
        ListElement { text: "Deuteronomy 34"; value: "DT34_0";}
    }
    ListModel {
        id: josmod
        ListElement { text: "Joshua 1"; value: "JS1_0";}
        ListElement { text: "Joshua 2"; value: "JS2_0";}
        ListElement { text: "Joshua 3"; value: "JS3_0";}
        ListElement { text: "Joshua 4"; value: "JS4_0";}
        ListElement { text: "Joshua 5"; value: "JS5_0";}
        ListElement { text: "Joshua 6"; value: "JS6_0";}
        ListElement { text: "Joshua 7"; value: "JS7_0";}
        ListElement { text: "Joshua 8"; value: "JS8_0";}
        ListElement { text: "Joshua 9"; value: "JS9_0";}
        ListElement { text: "Joshua 10"; value: "JS10_0";}
        ListElement { text: "Joshua 11"; value: "JS11_0";}
        ListElement { text: "Joshua 12"; value: "JS12_0";}
        ListElement { text: "Joshua 13"; value: "JS13_0";}
        ListElement { text: "Joshua 14"; value: "JS14_0";}
        ListElement { text: "Joshua 15"; value: "JS15_0";}
        ListElement { text: "Joshua 16"; value: "JS16_0";}
        ListElement { text: "Joshua 17"; value: "JS17_0";}
        ListElement { text: "Joshua 18"; value: "JS18_0";}
        ListElement { text: "Joshua 19"; value: "JS19_0";}
        ListElement { text: "Joshua 20"; value: "JS20_0";}
        ListElement { text: "Joshua 21"; value: "JS21_0";}
        ListElement { text: "Joshua 22"; value: "JS22_0";}
        ListElement { text: "Joshua 23"; value: "JS23_0";}
        ListElement { text: "Joshua 24"; value: "JS24_0";}
    }
    ListModel {
        id: jdgmod
        ListElement { text: "Judges 1"; value: "JG1_0";}
        ListElement { text: "Judges 2"; value: "JG2_0";}
        ListElement { text: "Judges 3"; value: "JG3_0";}
        ListElement { text: "Judges 4"; value: "JG4_0";}
        ListElement { text: "Judges 5"; value: "JG5_0";}
        ListElement { text: "Judges 6"; value: "JG6_0";}
        ListElement { text: "Judges 7"; value: "JG7_0";}
        ListElement { text: "Judges 8"; value: "JG8_0";}
        ListElement { text: "Judges 9"; value: "JG9_0";}
        ListElement { text: "Judges 10"; value: "JG10_0";}
        ListElement { text: "Judges 11"; value: "JG11_0";}
        ListElement { text: "Judges 12"; value: "JG12_0";}
        ListElement { text: "Judges 13"; value: "JG13_0";}
        ListElement { text: "Judges 14"; value: "JG14_0";}
        ListElement { text: "Judges 15"; value: "JG15_0";}
        ListElement { text: "Judges 16"; value: "JG16_0";}
        ListElement { text: "Judges 17"; value: "JG17_0";}
        ListElement { text: "Judges 18"; value: "JG18_0";}
        ListElement { text: "Judges 19"; value: "JG19_0";}
        ListElement { text: "Judges 20"; value: "JG20_0";}
        ListElement { text: "Judges 21"; value: "JG21_0";}
    }
    ListModel {
        id: rutmod
        ListElement { text: "Ruth 1"; value: "RT1_0";}
        ListElement { text: "Ruth 2"; value: "RT2_0";}
        ListElement { text: "Ruth 3"; value: "RT3_0";}
        ListElement { text: "Ruth 4"; value: "RT4_0";}
    }
    ListModel {
        id: onesamod
        ListElement { text: "1 Samuel 1"; value: "S11_0";}
        ListElement { text: "1 Samuel 2"; value: "S12_0";}
        ListElement { text: "1 Samuel 3"; value: "S13_0";}
        ListElement { text: "1 Samuel 4"; value: "S14_0";}
        ListElement { text: "1 Samuel 5"; value: "S15_0";}
        ListElement { text: "1 Samuel 6"; value: "S16_0";}
        ListElement { text: "1 Samuel 7"; value: "S17_0";}
        ListElement { text: "1 Samuel 8"; value: "S18_0";}
        ListElement { text: "1 Samuel 9"; value: "S19_0";}
        ListElement { text: "1 Samuel 10"; value: "S110_0";}
        ListElement { text: "1 Samuel 11"; value: "S111_0";}
        ListElement { text: "1 Samuel 12"; value: "S112_0";}
        ListElement { text: "1 Samuel 13"; value: "S113_0";}
        ListElement { text: "1 Samuel 14"; value: "S114_0";}
        ListElement { text: "1 Samuel 15"; value: "S115_0";}
        ListElement { text: "1 Samuel 16"; value: "S116_0";}
        ListElement { text: "1 Samuel 17"; value: "S117_0";}
        ListElement { text: "1 Samuel 18"; value: "S118_0";}
        ListElement { text: "1 Samuel 19"; value: "S119_0";}
        ListElement { text: "1 Samuel 20"; value: "S120_0";}
        ListElement { text: "1 Samuel 21"; value: "S121_0";}
        ListElement { text: "1 Samuel 22"; value: "S122_0";}
        ListElement { text: "1 Samuel 23"; value: "S123_0";}
        ListElement { text: "1 Samuel 24"; value: "S124_0";}
        ListElement { text: "1 Samuel 25"; value: "S125_0";}
        ListElement { text: "1 Samuel 26"; value: "S126_0";}
        ListElement { text: "1 Samuel 27"; value: "S127_0";}
        ListElement { text: "1 Samuel 28"; value: "S128_0";}
        ListElement { text: "1 Samuel 29"; value: "S129_0";}
        ListElement { text: "1 Samuel 30"; value: "S130_0";}
        ListElement { text: "1 Samuel 31"; value: "S131_0";}
    }
    ListModel {
        id: twosamod
        ListElement { text: "2 Samuel 1"; value: "S21_0";}
        ListElement { text: "2 Samuel 2"; value: "S22_0";}
        ListElement { text: "2 Samuel 3"; value: "S23_0";}
        ListElement { text: "2 Samuel 4"; value: "S24_0";}
        ListElement { text: "2 Samuel 5"; value: "S25_0";}
        ListElement { text: "2 Samuel 6"; value: "S26_0";}
        ListElement { text: "2 Samuel 7"; value: "S27_0";}
        ListElement { text: "2 Samuel 8"; value: "S28_0";}
        ListElement { text: "2 Samuel 9"; value: "S29_0";}
        ListElement { text: "2 Samuel 10"; value: "S210_0";}
        ListElement { text: "2 Samuel 11"; value: "S211_0";}
        ListElement { text: "2 Samuel 12"; value: "S212_0";}
        ListElement { text: "2 Samuel 13"; value: "S213_0";}
        ListElement { text: "2 Samuel 14"; value: "S214_0";}
        ListElement { text: "2 Samuel 15"; value: "S215_0";}
        ListElement { text: "2 Samuel 16"; value: "S216_0";}
        ListElement { text: "2 Samuel 17"; value: "S217_0";}
        ListElement { text: "2 Samuel 18"; value: "S218_0";}
        ListElement { text: "2 Samuel 19"; value: "S219_0";}
        ListElement { text: "2 Samuel 20"; value: "S220_0";}
        ListElement { text: "2 Samuel 21"; value: "S221_0";}
        ListElement { text: "2 Samuel 22"; value: "S222_0";}
        ListElement { text: "2 Samuel 23"; value: "S223_0";}
        ListElement { text: "2 Samuel 24"; value: "S224_0";}
    }
    ListModel {
        id: onekimod
        ListElement { text: "1 Kings 1"; value: "K11_0";}
        ListElement { text: "1 Kings 2"; value: "K12_0";}
        ListElement { text: "1 Kings 3"; value: "K13_0";}
        ListElement { text: "1 Kings 4"; value: "K14_0";}
        ListElement { text: "1 Kings 5"; value: "K15_0";}
        ListElement { text: "1 Kings 6"; value: "K16_0";}
        ListElement { text: "1 Kings 7"; value: "K17_0";}
        ListElement { text: "1 Kings 8"; value: "K18_0";}
        ListElement { text: "1 Kings 9"; value: "K19_0";}
        ListElement { text: "1 Kings 10"; value: "K110_0";}
        ListElement { text: "1 Kings 11"; value: "K111_0";}
        ListElement { text: "1 Kings 12"; value: "K112_0";}
        ListElement { text: "1 Kings 13"; value: "K113_0";}
        ListElement { text: "1 Kings 14"; value: "K114_0";}
        ListElement { text: "1 Kings 15"; value: "K115_0";}
        ListElement { text: "1 Kings 16"; value: "K116_0";}
        ListElement { text: "1 Kings 17"; value: "K117_0";}
        ListElement { text: "1 Kings 18"; value: "K118_0";}
        ListElement { text: "1 Kings 19"; value: "K119_0";}
        ListElement { text: "1 Kings 20"; value: "K120_0";}
        ListElement { text: "1 Kings 21"; value: "K121_0";}
        ListElement { text: "1 Kings 22"; value: "K122_0";}
    }
    ListModel {
        id: twokimod
        ListElement { text: "2 Kings 1"; value: "K21_0";}
        ListElement { text: "2 Kings 2"; value: "K22_0";}
        ListElement { text: "2 Kings 3"; value: "K23_0";}
        ListElement { text: "2 Kings 4"; value: "K24_0";}
        ListElement { text: "2 Kings 5"; value: "K25_0";}
        ListElement { text: "2 Kings 6"; value: "K26_0";}
        ListElement { text: "2 Kings 7"; value: "K27_0";}
        ListElement { text: "2 Kings 8"; value: "K28_0";}
        ListElement { text: "2 Kings 9"; value: "K29_0";}
        ListElement { text: "2 Kings 10"; value: "K210_0";}
        ListElement { text: "2 Kings 11"; value: "K211_0";}
        ListElement { text: "2 Kings 12"; value: "K212_0";}
        ListElement { text: "2 Kings 13"; value: "K213_0";}
        ListElement { text: "2 Kings 14"; value: "K214_0";}
        ListElement { text: "2 Kings 15"; value: "K215_0";}
        ListElement { text: "2 Kings 16"; value: "K216_0";}
        ListElement { text: "2 Kings 17"; value: "K217_0";}
        ListElement { text: "2 Kings 18"; value: "K218_0";}
        ListElement { text: "2 Kings 19"; value: "K219_0";}
        ListElement { text: "2 Kings 20"; value: "K220_0";}
        ListElement { text: "2 Kings 21"; value: "K221_0";}
        ListElement { text: "2 Kings 22"; value: "K222_0";}
        ListElement { text: "2 Kings 23"; value: "K223_0";}
        ListElement { text: "2 Kings 24"; value: "K224_0";}
        ListElement { text: "2 Kings 25"; value: "K225_0";}
    }
    ListModel {
        id: onechmod
        ListElement { text: "1 Chronicles 1"; value: "R11_0";}
        ListElement { text: "1 Chronicles 2"; value: "R12_0";}
        ListElement { text: "1 Chronicles 3"; value: "R13_0";}
        ListElement { text: "1 Chronicles 4"; value: "R14_0";}
        ListElement { text: "1 Chronicles 5"; value: "R15_0";}
        ListElement { text: "1 Chronicles 6"; value: "R16_0";}
        ListElement { text: "1 Chronicles 7"; value: "R17_0";}
        ListElement { text: "1 Chronicles 8"; value: "R18_0";}
        ListElement { text: "1 Chronicles 9"; value: "R19_0";}
        ListElement { text: "1 Chronicles 10"; value: "R110_0";}
        ListElement { text: "1 Chronicles 11"; value: "R111_0";}
        ListElement { text: "1 Chronicles 12"; value: "R112_0";}
        ListElement { text: "1 Chronicles 13"; value: "R113_0";}
        ListElement { text: "1 Chronicles 14"; value: "R114_0";}
        ListElement { text: "1 Chronicles 15"; value: "R115_0";}
        ListElement { text: "1 Chronicles 16"; value: "R116_0";}
        ListElement { text: "1 Chronicles 17"; value: "R117_0";}
        ListElement { text: "1 Chronicles 18"; value: "R118_0";}
        ListElement { text: "1 Chronicles 19"; value: "R119_0";}
        ListElement { text: "1 Chronicles 20"; value: "R120_0";}
        ListElement { text: "1 Chronicles 21"; value: "R121_0";}
        ListElement { text: "1 Chronicles 22"; value: "R122_0";}
        ListElement { text: "1 Chronicles 23"; value: "R123_0";}
        ListElement { text: "1 Chronicles 24"; value: "R124_0";}
        ListElement { text: "1 Chronicles 25"; value: "R125_0";}
        ListElement { text: "1 Chronicles 26"; value: "R126_0";}
        ListElement { text: "1 Chronicles 27"; value: "R127_0";}
        ListElement { text: "1 Chronicles 28"; value: "R128_0";}
        ListElement { text: "1 Chronicles 29"; value: "R129_0";}
    }
    ListModel {
        id: twochmod
        ListElement { text: "2 Chronicles 1"; value: "R21_0";}
        ListElement { text: "2 Chronicles 2"; value: "R22_0";}
        ListElement { text: "2 Chronicles 3"; value: "R23_0";}
        ListElement { text: "2 Chronicles 4"; value: "R24_0";}
        ListElement { text: "2 Chronicles 5"; value: "R25_0";}
        ListElement { text: "2 Chronicles 6"; value: "R26_0";}
        ListElement { text: "2 Chronicles 7"; value: "R27_0";}
        ListElement { text: "2 Chronicles 8"; value: "R28_0";}
        ListElement { text: "2 Chronicles 9"; value: "R29_0";}
        ListElement { text: "2 Chronicles 10"; value: "R210_0";}
        ListElement { text: "2 Chronicles 11"; value: "R211_0";}
        ListElement { text: "2 Chronicles 12"; value: "R212_0";}
        ListElement { text: "2 Chronicles 13"; value: "R213_0";}
        ListElement { text: "2 Chronicles 14"; value: "R214_0";}
        ListElement { text: "2 Chronicles 15"; value: "R215_0";}
        ListElement { text: "2 Chronicles 16"; value: "R216_0";}
        ListElement { text: "2 Chronicles 17"; value: "R217_0";}
        ListElement { text: "2 Chronicles 18"; value: "R218_0";}
        ListElement { text: "2 Chronicles 19"; value: "R219_0";}
        ListElement { text: "2 Chronicles 20"; value: "R220_0";}
        ListElement { text: "2 Chronicles 21"; value: "R221_0";}
        ListElement { text: "2 Chronicles 22"; value: "R222_0";}
        ListElement { text: "2 Chronicles 23"; value: "R223_0";}
        ListElement { text: "2 Chronicles 24"; value: "R224_0";}
        ListElement { text: "2 Chronicles 25"; value: "R225_0";}
        ListElement { text: "2 Chronicles 26"; value: "R226_0";}
        ListElement { text: "2 Chronicles 27"; value: "R227_0";}
        ListElement { text: "2 Chronicles 28"; value: "R228_0";}
        ListElement { text: "2 Chronicles 29"; value: "R229_0";}
        ListElement { text: "2 Chronicles 30"; value: "R230_0";}
        ListElement { text: "2 Chronicles 31"; value: "R231_0";}
        ListElement { text: "2 Chronicles 32"; value: "R232_0";}
        ListElement { text: "2 Chronicles 33"; value: "R233_0";}
        ListElement { text: "2 Chronicles 34"; value: "R234_0";}
        ListElement { text: "2 Chronicles 35"; value: "R235_0";}
        ListElement { text: "2 Chronicles 36"; value: "R236_0";}
    }
    ListModel {
        id: ezrmod
        ListElement { text: "Ezra 1"; value: "ER1_0";}
        ListElement { text: "Ezra 2"; value: "ER2_0";}
        ListElement { text: "Ezra 3"; value: "ER3_0";}
        ListElement { text: "Ezra 4"; value: "ER4_0";}
        ListElement { text: "Ezra 5"; value: "ER5_0";}
        ListElement { text: "Ezra 6"; value: "ER6_0";}
        ListElement { text: "Ezra 7"; value: "ER7_0";}
        ListElement { text: "Ezra 8"; value: "ER8_0";}
        ListElement { text: "Ezra 9"; value: "ER9_0";}
        ListElement { text: "Ezra 10"; value: "ER10_0";}
    }
    ListModel {
        id: nehmod
        ListElement { text: "Nehemiah 1"; value: "NH1_0";}
        ListElement { text: "Nehemiah 2"; value: "NH2_0";}
        ListElement { text: "Nehemiah 3"; value: "NH3_0";}
        ListElement { text: "Nehemiah 4"; value: "NH4_0";}
        ListElement { text: "Nehemiah 5"; value: "NH5_0";}
        ListElement { text: "Nehemiah 6"; value: "NH6_0";}
        ListElement { text: "Nehemiah 7"; value: "NH7_0";}
        ListElement { text: "Nehemiah 8"; value: "NH8_0";}
        ListElement { text: "Nehemiah 9"; value: "NH9_0";}
        ListElement { text: "Nehemiah 10"; value: "NH10_0";}
        ListElement { text: "Nehemiah 11"; value: "NH11_0";}
        ListElement { text: "Nehemiah 12"; value: "NH12_0";}
        ListElement { text: "Nehemiah 13"; value: "NH13_0";}
    }
    ListModel {
        id: estmod
        ListElement { text: "Esther 1"; value: "ET1_0";}
        ListElement { text: "Esther 2"; value: "ET2_0";}
        ListElement { text: "Esther 3"; value: "ET3_0";}
        ListElement { text: "Esther 4"; value: "ET4_0";}
        ListElement { text: "Esther 5"; value: "ET5_0";}
        ListElement { text: "Esther 6"; value: "ET6_0";}
        ListElement { text: "Esther 7"; value: "ET7_0";}
        ListElement { text: "Esther 8"; value: "ET8_0";}
        ListElement { text: "Esther 9"; value: "ET9_0";}
        ListElement { text: "Esther 10"; value: "ET10_0";}
    }
    ListModel {
        id: jobmod
        ListElement { text: "Job 1"; value: "JB1_0";}
        ListElement { text: "Job 2"; value: "JB2_0";}
        ListElement { text: "Job 3"; value: "JB3_0";}
        ListElement { text: "Job 4"; value: "JB4_0";}
        ListElement { text: "Job 5"; value: "JB5_0";}
        ListElement { text: "Job 6"; value: "JB6_0";}
        ListElement { text: "Job 7"; value: "JB7_0";}
        ListElement { text: "Job 8"; value: "JB8_0";}
        ListElement { text: "Job 9"; value: "JB9_0";}
        ListElement { text: "Job 10"; value: "JB10_0";}
        ListElement { text: "Job 11"; value: "JB11_0";}
        ListElement { text: "Job 12"; value: "JB12_0";}
        ListElement { text: "Job 13"; value: "JB13_0";}
        ListElement { text: "Job 14"; value: "JB14_0";}
        ListElement { text: "Job 15"; value: "JB15_0";}
        ListElement { text: "Job 16"; value: "JB16_0";}
        ListElement { text: "Job 17"; value: "JB17_0";}
        ListElement { text: "Job 18"; value: "JB18_0";}
        ListElement { text: "Job 19"; value: "JB19_0";}
        ListElement { text: "Job 20"; value: "JB20_0";}
        ListElement { text: "Job 21"; value: "JB21_0";}
        ListElement { text: "Job 22"; value: "JB22_0";}
        ListElement { text: "Job 23"; value: "JB23_0";}
        ListElement { text: "Job 24"; value: "JB24_0";}
        ListElement { text: "Job 25"; value: "JB25_0";}
        ListElement { text: "Job 26"; value: "JB26_0";}
        ListElement { text: "Job 27"; value: "JB27_0";}
        ListElement { text: "Job 28"; value: "JB28_0";}
        ListElement { text: "Job 29"; value: "JB29_0";}
        ListElement { text: "Job 30"; value: "JB30_0";}
        ListElement { text: "Job 31"; value: "JB31_0";}
        ListElement { text: "Job 32"; value: "JB32_0";}
        ListElement { text: "Job 33"; value: "JB33_0";}
        ListElement { text: "Job 34"; value: "JB34_0";}
        ListElement { text: "Job 35"; value: "JB35_0";}
        ListElement { text: "Job 36"; value: "JB36_0";}
        ListElement { text: "Job 37"; value: "JB37_0";}
        ListElement { text: "Job 38"; value: "JB38_0";}
        ListElement { text: "Job 39"; value: "JB39_0";}
        ListElement { text: "Job 40"; value: "JB40_0";}
        ListElement { text: "Job 41"; value: "JB41_0";}
        ListElement { text: "Job 42"; value: "JB42_0";}
    }
    ListModel {
        id: psamod
        ListElement { text: "Psalms 1"; value: "PS1_0";}
        ListElement { text: "Psalms 2"; value: "PS2_0";}
        ListElement { text: "Psalms 3"; value: "PS3_0";}
        ListElement { text: "Psalms 4"; value: "PS4_0";}
        ListElement { text: "Psalms 5"; value: "PS5_0";}
        ListElement { text: "Psalms 6"; value: "PS6_0";}
        ListElement { text: "Psalms 7"; value: "PS7_0";}
        ListElement { text: "Psalms 8"; value: "PS8_0";}
        ListElement { text: "Psalms 9"; value: "PS9_0";}
        ListElement { text: "Psalms 10"; value: "PS10_0";}
        ListElement { text: "Psalms 11"; value: "PS11_0";}
        ListElement { text: "Psalms 12"; value: "PS12_0";}
        ListElement { text: "Psalms 13"; value: "PS13_0";}
        ListElement { text: "Psalms 14"; value: "PS14_0";}
        ListElement { text: "Psalms 15"; value: "PS15_0";}
        ListElement { text: "Psalms 16"; value: "PS16_0";}
        ListElement { text: "Psalms 17"; value: "PS17_0";}
        ListElement { text: "Psalms 18"; value: "PS18_0";}
        ListElement { text: "Psalms 19"; value: "PS19_0";}
        ListElement { text: "Psalms 20"; value: "PS20_0";}
        ListElement { text: "Psalms 21"; value: "PS21_0";}
        ListElement { text: "Psalms 22"; value: "PS22_0";}
        ListElement { text: "Psalms 23"; value: "PS23_0";}
        ListElement { text: "Psalms 24"; value: "PS24_0";}
        ListElement { text: "Psalms 25"; value: "PS25_0";}
        ListElement { text: "Psalms 26"; value: "PS26_0";}
        ListElement { text: "Psalms 27"; value: "PS27_0";}
        ListElement { text: "Psalms 28"; value: "PS28_0";}
        ListElement { text: "Psalms 29"; value: "PS29_0";}
        ListElement { text: "Psalms 30"; value: "PS30_0";}
        ListElement { text: "Psalms 31"; value: "PS31_0";}
        ListElement { text: "Psalms 32"; value: "PS32_0";}
        ListElement { text: "Psalms 33"; value: "PS33_0";}
        ListElement { text: "Psalms 34"; value: "PS34_0";}
        ListElement { text: "Psalms 35"; value: "PS35_0";}
        ListElement { text: "Psalms 36"; value: "PS36_0";}
        ListElement { text: "Psalms 37"; value: "PS37_0";}
        ListElement { text: "Psalms 38"; value: "PS38_0";}
        ListElement { text: "Psalms 39"; value: "PS39_0";}
        ListElement { text: "Psalms 40"; value: "PS40_0";}
        ListElement { text: "Psalms 41"; value: "PS41_0";}
        ListElement { text: "Psalms 42"; value: "PS42_0";}
        ListElement { text: "Psalms 43"; value: "PS43_0";}
        ListElement { text: "Psalms 44"; value: "PS44_0";}
        ListElement { text: "Psalms 45"; value: "PS45_0";}
        ListElement { text: "Psalms 46"; value: "PS46_0";}
        ListElement { text: "Psalms 47"; value: "PS47_0";}
        ListElement { text: "Psalms 48"; value: "PS48_0";}
        ListElement { text: "Psalms 49"; value: "PS49_0";}
        ListElement { text: "Psalms 50"; value: "PS50_0";}
        ListElement { text: "Psalms 51"; value: "PS51_0";}
        ListElement { text: "Psalms 52"; value: "PS52_0";}
        ListElement { text: "Psalms 53"; value: "PS53_0";}
        ListElement { text: "Psalms 54"; value: "PS54_0";}
        ListElement { text: "Psalms 55"; value: "PS55_0";}
        ListElement { text: "Psalms 56"; value: "PS56_0";}
        ListElement { text: "Psalms 57"; value: "PS57_0";}
        ListElement { text: "Psalms 58"; value: "PS58_0";}
        ListElement { text: "Psalms 59"; value: "PS59_0";}
        ListElement { text: "Psalms 60"; value: "PS60_0";}
        ListElement { text: "Psalms 61"; value: "PS61_0";}
        ListElement { text: "Psalms 62"; value: "PS62_0";}
        ListElement { text: "Psalms 63"; value: "PS63_0";}
        ListElement { text: "Psalms 64"; value: "PS64_0";}
        ListElement { text: "Psalms 65"; value: "PS65_0";}
        ListElement { text: "Psalms 66"; value: "PS66_0";}
        ListElement { text: "Psalms 67"; value: "PS67_0";}
        ListElement { text: "Psalms 68"; value: "PS68_0";}
        ListElement { text: "Psalms 69"; value: "PS69_0";}
        ListElement { text: "Psalms 70"; value: "PS70_0";}
        ListElement { text: "Psalms 71"; value: "PS71_0";}
        ListElement { text: "Psalms 72"; value: "PS72_0";}
        ListElement { text: "Psalms 73"; value: "PS73_0";}
        ListElement { text: "Psalms 74"; value: "PS74_0";}
        ListElement { text: "Psalms 75"; value: "PS75_0";}
        ListElement { text: "Psalms 76"; value: "PS76_0";}
        ListElement { text: "Psalms 77"; value: "PS77_0";}
        ListElement { text: "Psalms 78"; value: "PS78_0";}
        ListElement { text: "Psalms 79"; value: "PS79_0";}
        ListElement { text: "Psalms 80"; value: "PS80_0";}
        ListElement { text: "Psalms 81"; value: "PS81_0";}
        ListElement { text: "Psalms 82"; value: "PS82_0";}
        ListElement { text: "Psalms 83"; value: "PS83_0";}
        ListElement { text: "Psalms 84"; value: "PS84_0";}
        ListElement { text: "Psalms 85"; value: "PS85_0";}
        ListElement { text: "Psalms 86"; value: "PS86_0";}
        ListElement { text: "Psalms 87"; value: "PS87_0";}
        ListElement { text: "Psalms 88"; value: "PS88_0";}
        ListElement { text: "Psalms 89"; value: "PS89_0";}
        ListElement { text: "Psalms 90"; value: "PS90_0";}
        ListElement { text: "Psalms 91"; value: "PS91_0";}
        ListElement { text: "Psalms 92"; value: "PS92_0";}
        ListElement { text: "Psalms 93"; value: "PS93_0";}
        ListElement { text: "Psalms 94"; value: "PS94_0";}
        ListElement { text: "Psalms 95"; value: "PS95_0";}
        ListElement { text: "Psalms 96"; value: "PS96_0";}
        ListElement { text: "Psalms 97"; value: "PS97_0";}
        ListElement { text: "Psalms 98"; value: "PS98_0";}
        ListElement { text: "Psalms 99"; value: "PS99_0";}
        ListElement { text: "Psalms 100"; value: "PS100_0";}
        ListElement { text: "Psalms 101"; value: "PS101_0";}
        ListElement { text: "Psalms 102"; value: "PS102_0";}
        ListElement { text: "Psalms 103"; value: "PS103_0";}
        ListElement { text: "Psalms 104"; value: "PS104_0";}
        ListElement { text: "Psalms 105"; value: "PS105_0";}
        ListElement { text: "Psalms 106"; value: "PS106_0";}
        ListElement { text: "Psalms 107"; value: "PS107_0";}
        ListElement { text: "Psalms 108"; value: "PS108_0";}
        ListElement { text: "Psalms 109"; value: "PS109_0";}
        ListElement { text: "Psalms 110"; value: "PS110_0";}
        ListElement { text: "Psalms 111"; value: "PS111_0";}
        ListElement { text: "Psalms 112"; value: "PS112_0";}
        ListElement { text: "Psalms 113"; value: "PS113_0";}
        ListElement { text: "Psalms 114"; value: "PS114_0";}
        ListElement { text: "Psalms 115"; value: "PS115_0";}
        ListElement { text: "Psalms 116"; value: "PS116_0";}
        ListElement { text: "Psalms 117"; value: "PS117_0";}
        ListElement { text: "Psalms 118"; value: "PS118_0";}
        ListElement { text: "Psalms 119"; value: "PS119_0";}
        ListElement { text: "Psalms 120"; value: "PS120_0";}
        ListElement { text: "Psalms 121"; value: "PS121_0";}
        ListElement { text: "Psalms 122"; value: "PS122_0";}
        ListElement { text: "Psalms 123"; value: "PS123_0";}
        ListElement { text: "Psalms 124"; value: "PS124_0";}
        ListElement { text: "Psalms 125"; value: "PS125_0";}
        ListElement { text: "Psalms 126"; value: "PS126_0";}
        ListElement { text: "Psalms 127"; value: "PS127_0";}
        ListElement { text: "Psalms 128"; value: "PS128_0";}
        ListElement { text: "Psalms 129"; value: "PS129_0";}
        ListElement { text: "Psalms 130"; value: "PS130_0";}
        ListElement { text: "Psalms 131"; value: "PS131_0";}
        ListElement { text: "Psalms 132"; value: "PS132_0";}
        ListElement { text: "Psalms 133"; value: "PS133_0";}
        ListElement { text: "Psalms 134"; value: "PS134_0";}
        ListElement { text: "Psalms 135"; value: "PS135_0";}
        ListElement { text: "Psalms 136"; value: "PS136_0";}
        ListElement { text: "Psalms 137"; value: "PS137_0";}
        ListElement { text: "Psalms 138"; value: "PS138_0";}
        ListElement { text: "Psalms 139"; value: "PS139_0";}
        ListElement { text: "Psalms 140"; value: "PS140_0";}
        ListElement { text: "Psalms 141"; value: "PS141_0";}
        ListElement { text: "Psalms 142"; value: "PS142_0";}
        ListElement { text: "Psalms 143"; value: "PS143_0";}
        ListElement { text: "Psalms 144"; value: "PS144_0";}
        ListElement { text: "Psalms 145"; value: "PS145_0";}
        ListElement { text: "Psalms 146"; value: "PS146_0";}
        ListElement { text: "Psalms 147"; value: "PS147_0";}
        ListElement { text: "Psalms 148"; value: "PS148_0";}
        ListElement { text: "Psalms 149"; value: "PS149_0";}
        ListElement { text: "Psalms 150"; value: "PS150_0";}
    }
    ListModel {
        id: promod
        ListElement { text: "Proverbs 1"; value: "PR1_0";}
        ListElement { text: "Proverbs 2"; value: "PR2_0";}
        ListElement { text: "Proverbs 3"; value: "PR3_0";}
        ListElement { text: "Proverbs 4"; value: "PR4_0";}
        ListElement { text: "Proverbs 5"; value: "PR5_0";}
        ListElement { text: "Proverbs 6"; value: "PR6_0";}
        ListElement { text: "Proverbs 7"; value: "PR7_0";}
        ListElement { text: "Proverbs 8"; value: "PR8_0";}
        ListElement { text: "Proverbs 9"; value: "PR9_0";}
        ListElement { text: "Proverbs 10"; value: "PR10_0";}
        ListElement { text: "Proverbs 11"; value: "PR11_0";}
        ListElement { text: "Proverbs 12"; value: "PR12_0";}
        ListElement { text: "Proverbs 13"; value: "PR13_0";}
        ListElement { text: "Proverbs 14"; value: "PR14_0";}
        ListElement { text: "Proverbs 15"; value: "PR15_0";}
        ListElement { text: "Proverbs 16"; value: "PR16_0";}
        ListElement { text: "Proverbs 17"; value: "PR17_0";}
        ListElement { text: "Proverbs 18"; value: "PR18_0";}
        ListElement { text: "Proverbs 19"; value: "PR19_0";}
        ListElement { text: "Proverbs 20"; value: "PR20_0";}
        ListElement { text: "Proverbs 21"; value: "PR21_0";}
        ListElement { text: "Proverbs 22"; value: "PR22_0";}
        ListElement { text: "Proverbs 23"; value: "PR23_0";}
        ListElement { text: "Proverbs 24"; value: "PR24_0";}
        ListElement { text: "Proverbs 25"; value: "PR25_0";}
        ListElement { text: "Proverbs 26"; value: "PR26_0";}
        ListElement { text: "Proverbs 27"; value: "PR27_0";}
        ListElement { text: "Proverbs 28"; value: "PR28_0";}
        ListElement { text: "Proverbs 29"; value: "PR29_0";}
        ListElement { text: "Proverbs 30"; value: "PR30_0";}
        ListElement { text: "Proverbs 31"; value: "PR31_0";}
    }
    ListModel {
        id: eccmod
        ListElement { text: "Ecclesiastes 1"; value: "EC1_0";}
        ListElement { text: "Ecclesiastes 2"; value: "EC2_0";}
        ListElement { text: "Ecclesiastes 3"; value: "EC3_0";}
        ListElement { text: "Ecclesiastes 4"; value: "EC4_0";}
        ListElement { text: "Ecclesiastes 5"; value: "EC5_0";}
        ListElement { text: "Ecclesiastes 6"; value: "EC6_0";}
        ListElement { text: "Ecclesiastes 7"; value: "EC7_0";}
        ListElement { text: "Ecclesiastes 8"; value: "EC8_0";}
        ListElement { text: "Ecclesiastes 9"; value: "EC9_0";}
        ListElement { text: "Ecclesiastes 10"; value: "EC10_0";}
        ListElement { text: "Ecclesiastes 11"; value: "EC11_0";}
        ListElement { text: "Ecclesiastes 12"; value: "EC12_0";}
    }
    ListModel {
        id: sngmod
        ListElement { text: "Song of Solomon 1"; value: "SS1_0";}
        ListElement { text: "Song of Solomon 2"; value: "SS2_0";}
        ListElement { text: "Song of Solomon 3"; value: "SS3_0";}
        ListElement { text: "Song of Solomon 4"; value: "SS4_0";}
        ListElement { text: "Song of Solomon 5"; value: "SS5_0";}
        ListElement { text: "Song of Solomon 6"; value: "SS6_0";}
        ListElement { text: "Song of Solomon 7"; value: "SS7_0";}
        ListElement { text: "Song of Solomon 8"; value: "SS8_0";}
    }
    ListModel {
        id: isamod
        ListElement { text: "Isaiah 1"; value: "IS1_0";}
        ListElement { text: "Isaiah 2"; value: "IS2_0";}
        ListElement { text: "Isaiah 3"; value: "IS3_0";}
        ListElement { text: "Isaiah 4"; value: "IS4_0";}
        ListElement { text: "Isaiah 5"; value: "IS5_0";}
        ListElement { text: "Isaiah 6"; value: "IS6_0";}
        ListElement { text: "Isaiah 7"; value: "IS7_0";}
        ListElement { text: "Isaiah 8"; value: "IS8_0";}
        ListElement { text: "Isaiah 9"; value: "IS9_0";}
        ListElement { text: "Isaiah 10"; value: "IS10_0";}
        ListElement { text: "Isaiah 11"; value: "IS11_0";}
        ListElement { text: "Isaiah 12"; value: "IS12_0";}
        ListElement { text: "Isaiah 13"; value: "IS13_0";}
        ListElement { text: "Isaiah 14"; value: "IS14_0";}
        ListElement { text: "Isaiah 15"; value: "IS15_0";}
        ListElement { text: "Isaiah 16"; value: "IS16_0";}
        ListElement { text: "Isaiah 17"; value: "IS17_0";}
        ListElement { text: "Isaiah 18"; value: "IS18_0";}
        ListElement { text: "Isaiah 19"; value: "IS19_0";}
        ListElement { text: "Isaiah 20"; value: "IS20_0";}
        ListElement { text: "Isaiah 21"; value: "IS21_0";}
        ListElement { text: "Isaiah 22"; value: "IS22_0";}
        ListElement { text: "Isaiah 23"; value: "IS23_0";}
        ListElement { text: "Isaiah 24"; value: "IS24_0";}
        ListElement { text: "Isaiah 25"; value: "IS25_0";}
        ListElement { text: "Isaiah 26"; value: "IS26_0";}
        ListElement { text: "Isaiah 27"; value: "IS27_0";}
        ListElement { text: "Isaiah 28"; value: "IS28_0";}
        ListElement { text: "Isaiah 29"; value: "IS29_0";}
        ListElement { text: "Isaiah 30"; value: "IS30_0";}
        ListElement { text: "Isaiah 31"; value: "IS31_0";}
        ListElement { text: "Isaiah 32"; value: "IS32_0";}
        ListElement { text: "Isaiah 33"; value: "IS33_0";}
        ListElement { text: "Isaiah 34"; value: "IS34_0";}
        ListElement { text: "Isaiah 35"; value: "IS35_0";}
        ListElement { text: "Isaiah 36"; value: "IS36_0";}
        ListElement { text: "Isaiah 37"; value: "IS37_0";}
        ListElement { text: "Isaiah 38"; value: "IS38_0";}
        ListElement { text: "Isaiah 39"; value: "IS39_0";}
        ListElement { text: "Isaiah 40"; value: "IS40_0";}
        ListElement { text: "Isaiah 41"; value: "IS41_0";}
        ListElement { text: "Isaiah 42"; value: "IS42_0";}
        ListElement { text: "Isaiah 43"; value: "IS43_0";}
        ListElement { text: "Isaiah 44"; value: "IS44_0";}
        ListElement { text: "Isaiah 45"; value: "IS45_0";}
        ListElement { text: "Isaiah 46"; value: "IS46_0";}
        ListElement { text: "Isaiah 47"; value: "IS47_0";}
        ListElement { text: "Isaiah 48"; value: "IS48_0";}
        ListElement { text: "Isaiah 49"; value: "IS49_0";}
        ListElement { text: "Isaiah 50"; value: "IS50_0";}
        ListElement { text: "Isaiah 51"; value: "IS51_0";}
        ListElement { text: "Isaiah 52"; value: "IS52_0";}
        ListElement { text: "Isaiah 53"; value: "IS53_0";}
        ListElement { text: "Isaiah 54"; value: "IS54_0";}
        ListElement { text: "Isaiah 55"; value: "IS55_0";}
        ListElement { text: "Isaiah 56"; value: "IS56_0";}
        ListElement { text: "Isaiah 57"; value: "IS57_0";}
        ListElement { text: "Isaiah 58"; value: "IS58_0";}
        ListElement { text: "Isaiah 59"; value: "IS59_0";}
        ListElement { text: "Isaiah 60"; value: "IS60_0";}
        ListElement { text: "Isaiah 61"; value: "IS61_0";}
        ListElement { text: "Isaiah 62"; value: "IS62_0";}
        ListElement { text: "Isaiah 63"; value: "IS63_0";}
        ListElement { text: "Isaiah 64"; value: "IS64_0";}
        ListElement { text: "Isaiah 65"; value: "IS65_0";}
        ListElement { text: "Isaiah 66"; value: "IS66_0";}
    }
    ListModel {
        id: jermod
        ListElement { text: "Jeremiah 1"; value: "JR1_0";}
        ListElement { text: "Jeremiah 2"; value: "JR2_0";}
        ListElement { text: "Jeremiah 3"; value: "JR3_0";}
        ListElement { text: "Jeremiah 4"; value: "JR4_0";}
        ListElement { text: "Jeremiah 5"; value: "JR5_0";}
        ListElement { text: "Jeremiah 6"; value: "JR6_0";}
        ListElement { text: "Jeremiah 7"; value: "JR7_0";}
        ListElement { text: "Jeremiah 8"; value: "JR8_0";}
        ListElement { text: "Jeremiah 9"; value: "JR9_0";}
        ListElement { text: "Jeremiah 10"; value: "JR10_0";}
        ListElement { text: "Jeremiah 11"; value: "JR11_0";}
        ListElement { text: "Jeremiah 12"; value: "JR12_0";}
        ListElement { text: "Jeremiah 13"; value: "JR13_0";}
        ListElement { text: "Jeremiah 14"; value: "JR14_0";}
        ListElement { text: "Jeremiah 15"; value: "JR15_0";}
        ListElement { text: "Jeremiah 16"; value: "JR16_0";}
        ListElement { text: "Jeremiah 17"; value: "JR17_0";}
        ListElement { text: "Jeremiah 18"; value: "JR18_0";}
        ListElement { text: "Jeremiah 19"; value: "JR19_0";}
        ListElement { text: "Jeremiah 20"; value: "JR20_0";}
        ListElement { text: "Jeremiah 21"; value: "JR21_0";}
        ListElement { text: "Jeremiah 22"; value: "JR22_0";}
        ListElement { text: "Jeremiah 23"; value: "JR23_0";}
        ListElement { text: "Jeremiah 24"; value: "JR24_0";}
        ListElement { text: "Jeremiah 25"; value: "JR25_0";}
        ListElement { text: "Jeremiah 26"; value: "JR26_0";}
        ListElement { text: "Jeremiah 27"; value: "JR27_0";}
        ListElement { text: "Jeremiah 28"; value: "JR28_0";}
        ListElement { text: "Jeremiah 29"; value: "JR29_0";}
        ListElement { text: "Jeremiah 30"; value: "JR30_0";}
        ListElement { text: "Jeremiah 31"; value: "JR31_0";}
        ListElement { text: "Jeremiah 32"; value: "JR32_0";}
        ListElement { text: "Jeremiah 33"; value: "JR33_0";}
        ListElement { text: "Jeremiah 34"; value: "JR34_0";}
        ListElement { text: "Jeremiah 35"; value: "JR35_0";}
        ListElement { text: "Jeremiah 36"; value: "JR36_0";}
        ListElement { text: "Jeremiah 37"; value: "JR37_0";}
        ListElement { text: "Jeremiah 38"; value: "JR38_0";}
        ListElement { text: "Jeremiah 39"; value: "JR39_0";}
        ListElement { text: "Jeremiah 40"; value: "JR40_0";}
        ListElement { text: "Jeremiah 41"; value: "JR41_0";}
        ListElement { text: "Jeremiah 42"; value: "JR42_0";}
        ListElement { text: "Jeremiah 43"; value: "JR43_0";}
        ListElement { text: "Jeremiah 44"; value: "JR44_0";}
        ListElement { text: "Jeremiah 45"; value: "JR45_0";}
        ListElement { text: "Jeremiah 46"; value: "JR46_0";}
        ListElement { text: "Jeremiah 47"; value: "JR47_0";}
        ListElement { text: "Jeremiah 48"; value: "JR48_0";}
        ListElement { text: "Jeremiah 49"; value: "JR49_0";}
        ListElement { text: "Jeremiah 50"; value: "JR50_0";}
        ListElement { text: "Jeremiah 51"; value: "JR51_0";}
        ListElement { text: "Jeremiah 52"; value: "JR52_0";}
    }
    ListModel {
        id: lammod
        ListElement { text: "Lamentations 1"; value: "LM1_0";}
        ListElement { text: "Lamentations 2"; value: "LM2_0";}
        ListElement { text: "Lamentations 3"; value: "LM3_0";}
        ListElement { text: "Lamentations 4"; value: "LM4_0";}
        ListElement { text: "Lamentations 5"; value: "LM5_0";}
    }
    ListModel {
        id: ezkmod
        ListElement { text: "Ezekiel 1"; value: "EK1_0";}
        ListElement { text: "Ezekiel 2"; value: "EK2_0";}
        ListElement { text: "Ezekiel 3"; value: "EK3_0";}
        ListElement { text: "Ezekiel 4"; value: "EK4_0";}
        ListElement { text: "Ezekiel 5"; value: "EK5_0";}
        ListElement { text: "Ezekiel 6"; value: "EK6_0";}
        ListElement { text: "Ezekiel 7"; value: "EK7_0";}
        ListElement { text: "Ezekiel 8"; value: "EK8_0";}
        ListElement { text: "Ezekiel 9"; value: "EK9_0";}
        ListElement { text: "Ezekiel 10"; value: "EK10_0";}
        ListElement { text: "Ezekiel 11"; value: "EK11_0";}
        ListElement { text: "Ezekiel 12"; value: "EK12_0";}
        ListElement { text: "Ezekiel 13"; value: "EK13_0";}
        ListElement { text: "Ezekiel 14"; value: "EK14_0";}
        ListElement { text: "Ezekiel 15"; value: "EK15_0";}
        ListElement { text: "Ezekiel 16"; value: "EK16_0";}
        ListElement { text: "Ezekiel 17"; value: "EK17_0";}
        ListElement { text: "Ezekiel 18"; value: "EK18_0";}
        ListElement { text: "Ezekiel 19"; value: "EK19_0";}
        ListElement { text: "Ezekiel 20"; value: "EK20_0";}
        ListElement { text: "Ezekiel 21"; value: "EK21_0";}
        ListElement { text: "Ezekiel 22"; value: "EK22_0";}
        ListElement { text: "Ezekiel 23"; value: "EK23_0";}
        ListElement { text: "Ezekiel 24"; value: "EK24_0";}
        ListElement { text: "Ezekiel 25"; value: "EK25_0";}
        ListElement { text: "Ezekiel 26"; value: "EK26_0";}
        ListElement { text: "Ezekiel 27"; value: "EK27_0";}
        ListElement { text: "Ezekiel 28"; value: "EK28_0";}
        ListElement { text: "Ezekiel 29"; value: "EK29_0";}
        ListElement { text: "Ezekiel 30"; value: "EK30_0";}
        ListElement { text: "Ezekiel 31"; value: "EK31_0";}
        ListElement { text: "Ezekiel 32"; value: "EK32_0";}
        ListElement { text: "Ezekiel 33"; value: "EK33_0";}
        ListElement { text: "Ezekiel 34"; value: "EK34_0";}
        ListElement { text: "Ezekiel 35"; value: "EK35_0";}
        ListElement { text: "Ezekiel 36"; value: "EK36_0";}
        ListElement { text: "Ezekiel 37"; value: "EK37_0";}
        ListElement { text: "Ezekiel 38"; value: "EK38_0";}
        ListElement { text: "Ezekiel 39"; value: "EK39_0";}
        ListElement { text: "Ezekiel 40"; value: "EK40_0";}
        ListElement { text: "Ezekiel 41"; value: "EK41_0";}
        ListElement { text: "Ezekiel 42"; value: "EK42_0";}
        ListElement { text: "Ezekiel 43"; value: "EK43_0";}
        ListElement { text: "Ezekiel 44"; value: "EK44_0";}
        ListElement { text: "Ezekiel 45"; value: "EK45_0";}
        ListElement { text: "Ezekiel 46"; value: "EK46_0";}
        ListElement { text: "Ezekiel 47"; value: "EK47_0";}
        ListElement { text: "Ezekiel 48"; value: "EK48_0";}
    }
    ListModel {
        id: danmod
        ListElement { text: "Daniel 1"; value: "DN1_0";}
        ListElement { text: "Daniel 2"; value: "DN2_0";}
        ListElement { text: "Daniel 3"; value: "DN3_0";}
        ListElement { text: "Daniel 4"; value: "DN4_0";}
        ListElement { text: "Daniel 5"; value: "DN5_0";}
        ListElement { text: "Daniel 6"; value: "DN6_0";}
        ListElement { text: "Daniel 7"; value: "DN7_0";}
        ListElement { text: "Daniel 8"; value: "DN8_0";}
        ListElement { text: "Daniel 9"; value: "DN9_0";}
        ListElement { text: "Daniel 10"; value: "DN10_0";}
        ListElement { text: "Daniel 11"; value: "DN11_0";}
        ListElement { text: "Daniel 12"; value: "DN12_0";}
    }
    ListModel {
        id: hosmod
        ListElement { text: "Hosea 1"; value: "HS1_0";}
        ListElement { text: "Hosea 2"; value: "HS2_0";}
        ListElement { text: "Hosea 3"; value: "HS3_0";}
        ListElement { text: "Hosea 4"; value: "HS4_0";}
        ListElement { text: "Hosea 5"; value: "HS5_0";}
        ListElement { text: "Hosea 6"; value: "HS6_0";}
        ListElement { text: "Hosea 7"; value: "HS7_0";}
        ListElement { text: "Hosea 8"; value: "HS8_0";}
        ListElement { text: "Hosea 9"; value: "HS9_0";}
        ListElement { text: "Hosea 10"; value: "HS10_0";}
        ListElement { text: "Hosea 11"; value: "HS11_0";}
        ListElement { text: "Hosea 12"; value: "HS12_0";}
        ListElement { text: "Hosea 13"; value: "HS13_0";}
        ListElement { text: "Hosea 14"; value: "HS14_0";}
    }
    ListModel {
        id: jolmod
        ListElement { text: "Joel 1"; value: "JL1_0";}
        ListElement { text: "Joel 2"; value: "JL2_0";}
        ListElement { text: "Joel 3"; value: "JL3_0";}
    }
    ListModel {
        id: amomod
        ListElement { text: "Amos 1"; value: "AM1_0";}
        ListElement { text: "Amos 2"; value: "AM2_0";}
        ListElement { text: "Amos 3"; value: "AM3_0";}
        ListElement { text: "Amos 4"; value: "AM4_0";}
        ListElement { text: "Amos 5"; value: "AM5_0";}
        ListElement { text: "Amos 6"; value: "AM6_0";}
        ListElement { text: "Amos 7"; value: "AM7_0";}
        ListElement { text: "Amos 8"; value: "AM8_0";}
        ListElement { text: "Amos 9"; value: "AM9_0";}
    }
    ListModel {
        id: obamod
        ListElement { text: "Obadiah 1"; value: "OB1_0";}
    }
    ListModel {
        id: jonmod
        ListElement { text: "Jonah 1"; value: "JH1_0";}
        ListElement { text: "Jonah 2"; value: "JH2_0";}
        ListElement { text: "Jonah 3"; value: "JH3_0";}
        ListElement { text: "Jonah 4"; value: "JH4_0";}
    }
    ListModel {
        id: micmod
        ListElement { text: "Micah 1"; value: "MC1_0";}
        ListElement { text: "Micah 2"; value: "MC2_0";}
        ListElement { text: "Micah 3"; value: "MC3_0";}
        ListElement { text: "Micah 4"; value: "MC4_0";}
        ListElement { text: "Micah 5"; value: "MC5_0";}
        ListElement { text: "Micah 6"; value: "MC6_0";}
        ListElement { text: "Micah 7"; value: "MC7_0";}
    }
    ListModel {
        id: nammod
        ListElement { text: "Nahum 1"; value: "NM1_0";}
        ListElement { text: "Nahum 2"; value: "NM2_0";}
        ListElement { text: "Nahum 3"; value: "NM3_0";}
    }
    ListModel {
        id: habmod
        ListElement { text: "Habakkuk 1"; value: "HK1_0";}
        ListElement { text: "Habakkuk 2"; value: "HK2_0";}
        ListElement { text: "Habakkuk 3"; value: "HK3_0";}
    }
    ListModel {
        id: zepmod
        ListElement { text: "Zephaniah 1"; value: "ZP1_0";}
        ListElement { text: "Zephaniah 2"; value: "ZP2_0";}
        ListElement { text: "Zephaniah 3"; value: "ZP3_0";}
    }
    ListModel {
        id: hagmod
        ListElement { text: "Haggai 1"; value: "HG1_0";}
        ListElement { text: "Haggai 2"; value: "HG2_0";}
    }
    ListModel {
        id: zecmod
        ListElement { text: "Zechariah 1"; value: "ZC1_0";}
        ListElement { text: "Zechariah 2"; value: "ZC2_0";}
        ListElement { text: "Zechariah 3"; value: "ZC3_0";}
        ListElement { text: "Zechariah 4"; value: "ZC4_0";}
        ListElement { text: "Zechariah 5"; value: "ZC5_0";}
        ListElement { text: "Zechariah 6"; value: "ZC6_0";}
        ListElement { text: "Zechariah 7"; value: "ZC7_0";}
        ListElement { text: "Zechariah 8"; value: "ZC8_0";}
        ListElement { text: "Zechariah 9"; value: "ZC9_0";}
        ListElement { text: "Zechariah 10"; value: "ZC10_0";}
        ListElement { text: "Zechariah 11"; value: "ZC11_0";}
        ListElement { text: "Zechariah 12"; value: "ZC12_0";}
        ListElement { text: "Zechariah 13"; value: "ZC13_0";}
        ListElement { text: "Zechariah 14"; value: "ZC14_0";}
    }
    ListModel {
        id: malmod
        ListElement { text: "Malachi 1"; value: "ML1_0";}
        ListElement { text: "Malachi 2"; value: "ML2_0";}
        ListElement { text: "Malachi 3"; value: "ML3_0";}
    }
    ListModel {
        id: matmod
        ListElement { text: "Matthew 1"; value: "MT1_0";}
        ListElement { text: "Matthew 2"; value: "MT2_0";}
        ListElement { text: "Matthew 3"; value: "MT3_0";}
        ListElement { text: "Matthew 4"; value: "MT4_0";}
        ListElement { text: "Matthew 5"; value: "MT5_0";}
        ListElement { text: "Matthew 6"; value: "MT6_0";}
        ListElement { text: "Matthew 7"; value: "MT7_0";}
        ListElement { text: "Matthew 8"; value: "MT8_0";}
        ListElement { text: "Matthew 9"; value: "MT9_0";}
        ListElement { text: "Matthew 10"; value: "MT10_0";}
        ListElement { text: "Matthew 11"; value: "MT11_0";}
        ListElement { text: "Matthew 12"; value: "MT12_0";}
        ListElement { text: "Matthew 13"; value: "MT13_0";}
        ListElement { text: "Matthew 14"; value: "MT14_0";}
        ListElement { text: "Matthew 15"; value: "MT15_0";}
        ListElement { text: "Matthew 16"; value: "MT16_0";}
        ListElement { text: "Matthew 17"; value: "MT17_0";}
        ListElement { text: "Matthew 18"; value: "MT18_0";}
        ListElement { text: "Matthew 19"; value: "MT19_0";}
        ListElement { text: "Matthew 20"; value: "MT20_0";}
        ListElement { text: "Matthew 21"; value: "MT21_0";}
        ListElement { text: "Matthew 22"; value: "MT22_0";}
        ListElement { text: "Matthew 23"; value: "MT23_0";}
        ListElement { text: "Matthew 24"; value: "MT24_0";}
        ListElement { text: "Matthew 25"; value: "MT25_0";}
        ListElement { text: "Matthew 26"; value: "MT26_0";}
        ListElement { text: "Matthew 27"; value: "MT27_0";}
        ListElement { text: "Matthew 28"; value: "MT28_0";}
    }
    ListModel {
        id: mrkmod
        ListElement { text: "Mark 1"; value: "MK1_0";}
        ListElement { text: "Mark 2"; value: "MK2_0";}
        ListElement { text: "Mark 3"; value: "MK3_0";}
        ListElement { text: "Mark 4"; value: "MK4_0";}
        ListElement { text: "Mark 5"; value: "MK5_0";}
        ListElement { text: "Mark 6"; value: "MK6_0";}
        ListElement { text: "Mark 7"; value: "MK7_0";}
        ListElement { text: "Mark 8"; value: "MK8_0";}
        ListElement { text: "Mark 9"; value: "MK9_0";}
        ListElement { text: "Mark 10"; value: "MK10_0";}
        ListElement { text: "Mark 11"; value: "MK11_0";}
        ListElement { text: "Mark 12"; value: "MK12_0";}
        ListElement { text: "Mark 13"; value: "MK13_0";}
        ListElement { text: "Mark 14"; value: "MK14_0";}
        ListElement { text: "Mark 15"; value: "MK15_0";}
        ListElement { text: "Mark 16"; value: "MK16_0";}
    }
    ListModel {
        id: lukmod
        ListElement { text: "Luke 1"; value: "LK1_0";}
        ListElement { text: "Luke 2"; value: "LK2_0";}
        ListElement { text: "Luke 3"; value: "LK3_0";}
        ListElement { text: "Luke 4"; value: "LK4_0";}
        ListElement { text: "Luke 5"; value: "LK5_0";}
        ListElement { text: "Luke 6"; value: "LK6_0";}
        ListElement { text: "Luke 7"; value: "LK7_0";}
        ListElement { text: "Luke 8"; value: "LK8_0";}
        ListElement { text: "Luke 9"; value: "LK9_0";}
        ListElement { text: "Luke 10"; value: "LK10_0";}
        ListElement { text: "Luke 11"; value: "LK11_0";}
        ListElement { text: "Luke 12"; value: "LK12_0";}
        ListElement { text: "Luke 13"; value: "LK13_0";}
        ListElement { text: "Luke 14"; value: "LK14_0";}
        ListElement { text: "Luke 15"; value: "LK15_0";}
        ListElement { text: "Luke 16"; value: "LK16_0";}
        ListElement { text: "Luke 17"; value: "LK17_0";}
        ListElement { text: "Luke 18"; value: "LK18_0";}
        ListElement { text: "Luke 19"; value: "LK19_0";}
        ListElement { text: "Luke 20"; value: "LK20_0";}
        ListElement { text: "Luke 21"; value: "LK21_0";}
        ListElement { text: "Luke 22"; value: "LK22_0";}
        ListElement { text: "Luke 23"; value: "LK23_0";}
        ListElement { text: "Luke 24"; value: "LK24_0";}
    }
    ListModel {
        id: jhnmod
        ListElement { text: "John 1"; value: "JN1_0";}
        ListElement { text: "John 2"; value: "JN2_0";}
        ListElement { text: "John 3"; value: "JN3_0";}
        ListElement { text: "John 4"; value: "JN4_0";}
        ListElement { text: "John 5"; value: "JN5_0";}
        ListElement { text: "John 6"; value: "JN6_0";}
        ListElement { text: "John 7"; value: "JN7_0";}
        ListElement { text: "John 8"; value: "JN8_0";}
        ListElement { text: "John 9"; value: "JN9_0";}
        ListElement { text: "John 10"; value: "JN10_0";}
        ListElement { text: "John 11"; value: "JN11_0";}
        ListElement { text: "John 12"; value: "JN12_0";}
        ListElement { text: "John 13"; value: "JN13_0";}
        ListElement { text: "John 14"; value: "JN14_0";}
        ListElement { text: "John 15"; value: "JN15_0";}
        ListElement { text: "John 16"; value: "JN16_0";}
        ListElement { text: "John 17"; value: "JN17_0";}
        ListElement { text: "John 18"; value: "JN18_0";}
        ListElement { text: "John 19"; value: "JN19_0";}
        ListElement { text: "John 20"; value: "JN20_0";}
        ListElement { text: "John 21"; value: "JN21_0";}
    }
    ListModel {
        id: actmod
        ListElement { text: "Acts 1"; value: "AC1_0";}
        ListElement { text: "Acts 2"; value: "AC2_0";}
        ListElement { text: "Acts 3"; value: "AC3_0";}
        ListElement { text: "Acts 4"; value: "AC4_0";}
        ListElement { text: "Acts 5"; value: "AC5_0";}
        ListElement { text: "Acts 6"; value: "AC6_0";}
        ListElement { text: "Acts 7"; value: "AC7_0";}
        ListElement { text: "Acts 8"; value: "AC8_0";}
        ListElement { text: "Acts 9"; value: "AC9_0";}
        ListElement { text: "Acts 10"; value: "AC10_0";}
        ListElement { text: "Acts 11"; value: "AC11_0";}
        ListElement { text: "Acts 12"; value: "AC12_0";}
        ListElement { text: "Acts 13"; value: "AC13_0";}
        ListElement { text: "Acts 14"; value: "AC14_0";}
        ListElement { text: "Acts 15"; value: "AC15_0";}
        ListElement { text: "Acts 16"; value: "AC16_0";}
        ListElement { text: "Acts 17"; value: "AC17_0";}
        ListElement { text: "Acts 18"; value: "AC18_0";}
        ListElement { text: "Acts 19"; value: "AC19_0";}
        ListElement { text: "Acts 20"; value: "AC20_0";}
        ListElement { text: "Acts 21"; value: "AC21_0";}
        ListElement { text: "Acts 22"; value: "AC22_0";}
        ListElement { text: "Acts 23"; value: "AC23_0";}
        ListElement { text: "Acts 24"; value: "AC24_0";}
        ListElement { text: "Acts 25"; value: "AC25_0";}
        ListElement { text: "Acts 26"; value: "AC26_0";}
        ListElement { text: "Acts 27"; value: "AC27_0";}
        ListElement { text: "Acts 28"; value: "AC28_0";}
    }
    ListModel {
        id: rommod
        ListElement { text: "Romans 1"; value: "RM1_0";}
        ListElement { text: "Romans 2"; value: "RM2_0";}
        ListElement { text: "Romans 3"; value: "RM3_0";}
        ListElement { text: "Romans 4"; value: "RM4_0";}
        ListElement { text: "Romans 5"; value: "RM5_0";}
        ListElement { text: "Romans 6"; value: "RM6_0";}
        ListElement { text: "Romans 7"; value: "RM7_0";}
        ListElement { text: "Romans 8"; value: "RM8_0";}
        ListElement { text: "Romans 9"; value: "RM9_0";}
        ListElement { text: "Romans 10"; value: "RM10_0";}
        ListElement { text: "Romans 11"; value: "RM11_0";}
        ListElement { text: "Romans 12"; value: "RM12_0";}
        ListElement { text: "Romans 13"; value: "RM13_0";}
        ListElement { text: "Romans 14"; value: "RM14_0";}
        ListElement { text: "Romans 15"; value: "RM15_0";}
        ListElement { text: "Romans 16"; value: "RM16_0";}
    }
    ListModel {
        id: onecomod
        ListElement { text: "1 Corinthians 1"; value: "C11_0";}
        ListElement { text: "1 Corinthians 2"; value: "C12_0";}
        ListElement { text: "1 Corinthians 3"; value: "C13_0";}
        ListElement { text: "1 Corinthians 4"; value: "C14_0";}
        ListElement { text: "1 Corinthians 5"; value: "C15_0";}
        ListElement { text: "1 Corinthians 6"; value: "C16_0";}
        ListElement { text: "1 Corinthians 7"; value: "C17_0";}
        ListElement { text: "1 Corinthians 8"; value: "C18_0";}
        ListElement { text: "1 Corinthians 9"; value: "C19_0";}
        ListElement { text: "1 Corinthians 10"; value: "C110_0";}
        ListElement { text: "1 Corinthians 11"; value: "C111_0";}
        ListElement { text: "1 Corinthians 12"; value: "C112_0";}
        ListElement { text: "1 Corinthians 13"; value: "C113_0";}
        ListElement { text: "1 Corinthians 14"; value: "C114_0";}
        ListElement { text: "1 Corinthians 15"; value: "C115_0";}
        ListElement { text: "1 Corinthians 16"; value: "C116_0";}
    }
    ListModel {
        id: twocomod
        ListElement { text: "2 Corinthians 1"; value: "C21_0";}
        ListElement { text: "2 Corinthians 2"; value: "C22_0";}
        ListElement { text: "2 Corinthians 3"; value: "C23_0";}
        ListElement { text: "2 Corinthians 4"; value: "C24_0";}
        ListElement { text: "2 Corinthians 5"; value: "C25_0";}
        ListElement { text: "2 Corinthians 6"; value: "C26_0";}
        ListElement { text: "2 Corinthians 7"; value: "C27_0";}
        ListElement { text: "2 Corinthians 8"; value: "C28_0";}
        ListElement { text: "2 Corinthians 9"; value: "C29_0";}
        ListElement { text: "2 Corinthians 10"; value: "C210_0";}
        ListElement { text: "2 Corinthians 11"; value: "C211_0";}
        ListElement { text: "2 Corinthians 12"; value: "C212_0";}
        ListElement { text: "2 Corinthians 13"; value: "C213_0";}
    }
    ListModel {
        id: galmod
        ListElement { text: "Galatians 1"; value: "GL1_0";}
        ListElement { text: "Galatians 2"; value: "GL2_0";}
        ListElement { text: "Galatians 3"; value: "GL3_0";}
        ListElement { text: "Galatians 4"; value: "GL4_0";}
        ListElement { text: "Galatians 5"; value: "GL5_0";}
        ListElement { text: "Galatians 6"; value: "GL6_0";}
    }
    ListModel {
        id: ephmod
        ListElement { text: "Ephesians 1"; value: "EP1_0";}
        ListElement { text: "Ephesians 2"; value: "EP2_0";}
        ListElement { text: "Ephesians 3"; value: "EP3_0";}
        ListElement { text: "Ephesians 4"; value: "EP4_0";}
        ListElement { text: "Ephesians 5"; value: "EP5_0";}
        ListElement { text: "Ephesians 6"; value: "EP6_0";}
    }
    ListModel {
        id: phpmod
        ListElement { text: "Philippians 1"; value: "PP1_0";}
        ListElement { text: "Philippians 2"; value: "PP2_0";}
        ListElement { text: "Philippians 3"; value: "PP3_0";}
        ListElement { text: "Philippians 4"; value: "PP4_0";}
    }
    ListModel {
        id: colmod
        ListElement { text: "Colossians 1"; value: "CL1_0";}
        ListElement { text: "Colossians 2"; value: "CL2_0";}
        ListElement { text: "Colossians 3"; value: "CL3_0";}
        ListElement { text: "Colossians 4"; value: "CL4_0";}
    }
    ListModel {
        id: onethmod
        ListElement { text: "1 Thessalonians 1"; value: "H11_0";}
        ListElement { text: "1 Thessalonians 2"; value: "H12_0";}
        ListElement { text: "1 Thessalonians 3"; value: "H13_0";}
        ListElement { text: "1 Thessalonians 4"; value: "H14_0";}
        ListElement { text: "1 Thessalonians 5"; value: "H15_0";}
    }
    ListModel {
        id: twothmod
        ListElement { text: "2 Thessalonians 1"; value: "H21_0";}
        ListElement { text: "2 Thessalonians 2"; value: "H22_0";}
        ListElement { text: "2 Thessalonians 3"; value: "H23_0";}
    }
    ListModel {
        id: onetimod
        ListElement { text: "1 Timothy 1"; value: "T11_0";}
        ListElement { text: "1 Timothy 2"; value: "T12_0";}
        ListElement { text: "1 Timothy 3"; value: "T13_0";}
        ListElement { text: "1 Timothy 4"; value: "T14_0";}
        ListElement { text: "1 Timothy 5"; value: "T15_0";}
        ListElement { text: "1 Timothy 6"; value: "T16_0";}
    }
    ListModel {
        id: twotimod
        ListElement { text: "2 Timothy 1"; value: "T21_0";}
        ListElement { text: "2 Timothy 2"; value: "T22_0";}
        ListElement { text: "2 Timothy 3"; value: "T23_0";}
        ListElement { text: "2 Timothy 4"; value: "T24_0";}
    }
    ListModel {
        id: titmod
        ListElement { text: "Titus 1"; value: "TT1_0";}
        ListElement { text: "Titus 2"; value: "TT2_0";}
        ListElement { text: "Titus 3"; value: "TT3_0";}
    }
    ListModel {
        id: phmmod
        ListElement { text: "Philemon 1"; value: "PM1_0";}
    }
    ListModel {
        id: hebmod
        ListElement { text: "Hebrews 1"; value: "HB1_0";}
        ListElement { text: "Hebrews 2"; value: "HB2_0";}
        ListElement { text: "Hebrews 3"; value: "HB3_0";}
        ListElement { text: "Hebrews 4"; value: "HB4_0";}
        ListElement { text: "Hebrews 5"; value: "HB5_0";}
        ListElement { text: "Hebrews 6"; value: "HB6_0";}
        ListElement { text: "Hebrews 7"; value: "HB7_0";}
        ListElement { text: "Hebrews 8"; value: "HB8_0";}
        ListElement { text: "Hebrews 9"; value: "HB9_0";}
        ListElement { text: "Hebrews 10"; value: "HB10_0";}
        ListElement { text: "Hebrews 11"; value: "HB11_0";}
        ListElement { text: "Hebrews 12"; value: "HB12_0";}
        ListElement { text: "Hebrews 13"; value: "HB13_0";}
    }
    ListModel {
        id: jasmod
        ListElement { text: "James 1"; value: "JM1_0";}
        ListElement { text: "James 2"; value: "JM2_0";}
        ListElement { text: "James 3"; value: "JM3_0";}
        ListElement { text: "James 4"; value: "JM4_0";}
        ListElement { text: "James 5"; value: "JM5_0";}
    }
    ListModel {
        id: onepemod
        ListElement { text: "1 Peter 1"; value: "P11_0";}
        ListElement { text: "1 Peter 2"; value: "P12_0";}
        ListElement { text: "1 Peter 3"; value: "P13_0";}
        ListElement { text: "1 Peter 4"; value: "P14_0";}
        ListElement { text: "1 Peter 5"; value: "P15_0";}
    }
    ListModel {
        id: twopemod
        ListElement { text: "2 Peter 1"; value: "P21_0";}
        ListElement { text: "2 Peter 2"; value: "P22_0";}
        ListElement { text: "2 Peter 3"; value: "P23_0";}
    }
    ListModel {
        id: onejnmod
        ListElement { text: "1 John 1"; value: "J11_0";}
        ListElement { text: "1 John 2"; value: "J12_0";}
        ListElement { text: "1 John 3"; value: "J13_0";}
        ListElement { text: "1 John 4"; value: "J14_0";}
        ListElement { text: "1 John 5"; value: "J15_0";}
    }
    ListModel {
        id: twojnmod
        ListElement { text: "2 John 1"; value: "J21_0";}
    }
    ListModel {
        id: threejnmod
        ListElement { text: "3 John 1"; value: "J31_0";}
    }
    ListModel {
        id: judmod
        ListElement { text: "Jude 1"; value: "JD1_0";}
    }
    ListModel {
        id: revmod
        ListElement { text: "Revelation 1"; value: "RV1_0";}
        ListElement { text: "Revelation 2"; value: "RV2_0";}
        ListElement { text: "Revelation 3"; value: "RV3_0";}
        ListElement { text: "Revelation 4"; value: "RV4_0";}
        ListElement { text: "Revelation 5"; value: "RV5_0";}
        ListElement { text: "Revelation 6"; value: "RV6_0";}
        ListElement { text: "Revelation 7"; value: "RV7_0";}
        ListElement { text: "Revelation 8"; value: "RV8_0";}
        ListElement { text: "Revelation 9"; value: "RV9_0";}
        ListElement { text: "Revelation 10"; value: "RV10_0";}
        ListElement { text: "Revelation 11"; value: "RV11_0";}
        ListElement { text: "Revelation 12"; value: "RV12_0";}
        ListElement { text: "Revelation 13"; value: "RV13_0";}
        ListElement { text: "Revelation 14"; value: "RV14_0";}
        ListElement { text: "Revelation 15"; value: "RV15_0";}
        ListElement { text: "Revelation 16"; value: "RV16_0";}
        ListElement { text: "Revelation 17"; value: "RV17_0";}
        ListElement { text: "Revelation 18"; value: "RV18_0";}
        ListElement { text: "Revelation 19"; value: "RV19_0";}
        ListElement { text: "Revelation 20"; value: "RV20_0";}
        ListElement { text: "Revelation 21"; value: "RV21_0";}
        ListElement { text: "Revelation 22"; value: "RV22_0";}
    }
    ListModel {
        id: tobmod
        ListElement { text: "Tobit 1"; value: "TB1_0";}
        ListElement { text: "Tobit 2"; value: "TB2_0";}
        ListElement { text: "Tobit 3"; value: "TB3_0";}
        ListElement { text: "Tobit 4"; value: "TB4_0";}
        ListElement { text: "Tobit 5"; value: "TB5_0";}
        ListElement { text: "Tobit 6"; value: "TB6_0";}
        ListElement { text: "Tobit 7"; value: "TB7_0";}
        ListElement { text: "Tobit 8"; value: "TB8_0";}
        ListElement { text: "Tobit 9"; value: "TB9_0";}
        ListElement { text: "Tobit 10"; value: "TB10_0";}
        ListElement { text: "Tobit 11"; value: "TB11_0";}
        ListElement { text: "Tobit 12"; value: "TB12_0";}
        ListElement { text: "Tobit 13"; value: "TB13_0";}
        ListElement { text: "Tobit 14"; value: "TB14_0";}
    }
    ListModel {
        id: jdtmod
        ListElement { text: "Judith 1"; value: "JT1_0";}
        ListElement { text: "Judith 2"; value: "JT2_0";}
        ListElement { text: "Judith 3"; value: "JT3_0";}
        ListElement { text: "Judith 4"; value: "JT4_0";}
        ListElement { text: "Judith 5"; value: "JT5_0";}
        ListElement { text: "Judith 6"; value: "JT6_0";}
        ListElement { text: "Judith 7"; value: "JT7_0";}
        ListElement { text: "Judith 8"; value: "JT8_0";}
        ListElement { text: "Judith 9"; value: "JT9_0";}
        ListElement { text: "Judith 10"; value: "JT10_0";}
        ListElement { text: "Judith 11"; value: "JT11_0";}
        ListElement { text: "Judith 12"; value: "JT12_0";}
        ListElement { text: "Judith 13"; value: "JT13_0";}
        ListElement { text: "Judith 14"; value: "JT14_0";}
        ListElement { text: "Judith 15"; value: "JT15_0";}
        ListElement { text: "Judith 16"; value: "JT16_0";}
    }
    ListModel {
        id: esgmod
        ListElement { text: "Esther (Greek) 1"; value: "EG1_0";}
        ListElement { text: "Esther (Greek) 2"; value: "EG2_0";}
        ListElement { text: "Esther (Greek) 3"; value: "EG3_0";}
        ListElement { text: "Esther (Greek) 4"; value: "EG4_0";}
        ListElement { text: "Esther (Greek) 5"; value: "EG5_0";}
        ListElement { text: "Esther (Greek) 6"; value: "EG6_0";}
        ListElement { text: "Esther (Greek) 7"; value: "EG7_0";}
        ListElement { text: "Esther (Greek) 8"; value: "EG8_0";}
        ListElement { text: "Esther (Greek) 9"; value: "EG9_0";}
        ListElement { text: "Esther (Greek) 10"; value: "EG10_0";}
    }
    ListModel {
        id: wismod
        ListElement { text: "Wisdom of Solomon 1"; value: "WS1_0";}
        ListElement { text: "Wisdom of Solomon 2"; value: "WS2_0";}
        ListElement { text: "Wisdom of Solomon 3"; value: "WS3_0";}
        ListElement { text: "Wisdom of Solomon 4"; value: "WS4_0";}
        ListElement { text: "Wisdom of Solomon 5"; value: "WS5_0";}
        ListElement { text: "Wisdom of Solomon 6"; value: "WS6_0";}
        ListElement { text: "Wisdom of Solomon 7"; value: "WS7_0";}
        ListElement { text: "Wisdom of Solomon 8"; value: "WS8_0";}
        ListElement { text: "Wisdom of Solomon 9"; value: "WS9_0";}
        ListElement { text: "Wisdom of Solomon 10"; value: "WS10_0";}
        ListElement { text: "Wisdom of Solomon 11"; value: "WS11_0";}
        ListElement { text: "Wisdom of Solomon 12"; value: "WS12_0";}
        ListElement { text: "Wisdom of Solomon 13"; value: "WS13_0";}
        ListElement { text: "Wisdom of Solomon 14"; value: "WS14_0";}
        ListElement { text: "Wisdom of Solomon 15"; value: "WS15_0";}
        ListElement { text: "Wisdom of Solomon 16"; value: "WS16_0";}
        ListElement { text: "Wisdom of Solomon 17"; value: "WS17_0";}
        ListElement { text: "Wisdom of Solomon 18"; value: "WS18_0";}
        ListElement { text: "Wisdom of Solomon 19"; value: "WS19_0";}
    }
    ListModel {
        id: sirmod
        ListElement { text: "Sirach 1"; value: "SR1_0";}
        ListElement { text: "Sirach 2"; value: "SR2_0";}
        ListElement { text: "Sirach 3"; value: "SR3_0";}
        ListElement { text: "Sirach 4"; value: "SR4_0";}
        ListElement { text: "Sirach 5"; value: "SR5_0";}
        ListElement { text: "Sirach 6"; value: "SR6_0";}
        ListElement { text: "Sirach 7"; value: "SR7_0";}
        ListElement { text: "Sirach 8"; value: "SR8_0";}
        ListElement { text: "Sirach 9"; value: "SR9_0";}
        ListElement { text: "Sirach 10"; value: "SR10_0";}
        ListElement { text: "Sirach 11"; value: "SR11_0";}
        ListElement { text: "Sirach 12"; value: "SR12_0";}
        ListElement { text: "Sirach 13"; value: "SR13_0";}
        ListElement { text: "Sirach 14"; value: "SR14_0";}
        ListElement { text: "Sirach 15"; value: "SR15_0";}
        ListElement { text: "Sirach 16"; value: "SR16_0";}
        ListElement { text: "Sirach 17"; value: "SR17_0";}
        ListElement { text: "Sirach 18"; value: "SR18_0";}
        ListElement { text: "Sirach 19"; value: "SR19_0";}
        ListElement { text: "Sirach 20"; value: "SR20_0";}
        ListElement { text: "Sirach 21"; value: "SR21_0";}
        ListElement { text: "Sirach 22"; value: "SR22_0";}
        ListElement { text: "Sirach 23"; value: "SR23_0";}
        ListElement { text: "Sirach 24"; value: "SR24_0";}
        ListElement { text: "Sirach 25"; value: "SR25_0";}
        ListElement { text: "Sirach 26"; value: "SR26_0";}
        ListElement { text: "Sirach 27"; value: "SR27_0";}
        ListElement { text: "Sirach 28"; value: "SR28_0";}
        ListElement { text: "Sirach 29"; value: "SR29_0";}
        ListElement { text: "Sirach 30"; value: "SR30_0";}
        ListElement { text: "Sirach 31"; value: "SR31_0";}
        ListElement { text: "Sirach 32"; value: "SR32_0";}
        ListElement { text: "Sirach 33"; value: "SR33_0";}
        ListElement { text: "Sirach 34"; value: "SR34_0";}
        ListElement { text: "Sirach 35"; value: "SR35_0";}
        ListElement { text: "Sirach 36"; value: "SR36_0";}
        ListElement { text: "Sirach 37"; value: "SR37_0";}
        ListElement { text: "Sirach 38"; value: "SR38_0";}
        ListElement { text: "Sirach 39"; value: "SR39_0";}
        ListElement { text: "Sirach 40"; value: "SR40_0";}
        ListElement { text: "Sirach 41"; value: "SR41_0";}
        ListElement { text: "Sirach 42"; value: "SR42_0";}
        ListElement { text: "Sirach 43"; value: "SR43_0";}
        ListElement { text: "Sirach 44"; value: "SR44_0";}
        ListElement { text: "Sirach 45"; value: "SR45_0";}
        ListElement { text: "Sirach 46"; value: "SR46_0";}
        ListElement { text: "Sirach 47"; value: "SR47_0";}
        ListElement { text: "Sirach 48"; value: "SR48_0";}
        ListElement { text: "Sirach 49"; value: "SR49_0";}
        ListElement { text: "Sirach 50"; value: "SR50_0";}
        ListElement { text: "Sirach 51"; value: "SR51_0";}
    }
    ListModel {
        id: barmod
        ListElement { text: "Baruch 1"; value: "BR1_0";}
        ListElement { text: "Baruch 2"; value: "BR2_0";}
        ListElement { text: "Baruch 3"; value: "BR3_0";}
        ListElement { text: "Baruch 4"; value: "BR4_0";}
        ListElement { text: "Baruch 5"; value: "BR5_0";}
        ListElement { text: "Baruch 6"; value: "BR6_0";}
    }
    ListModel {
        id: onemamod
        ListElement { text: "1 Maccabees 1"; value: "M11_0";}
        ListElement { text: "1 Maccabees 2"; value: "M12_0";}
        ListElement { text: "1 Maccabees 3"; value: "M13_0";}
        ListElement { text: "1 Maccabees 4"; value: "M14_0";}
        ListElement { text: "1 Maccabees 5"; value: "M15_0";}
        ListElement { text: "1 Maccabees 6"; value: "M16_0";}
        ListElement { text: "1 Maccabees 7"; value: "M17_0";}
        ListElement { text: "1 Maccabees 8"; value: "M18_0";}
        ListElement { text: "1 Maccabees 9"; value: "M19_0";}
        ListElement { text: "1 Maccabees 10"; value: "M110_0";}
        ListElement { text: "1 Maccabees 11"; value: "M111_0";}
        ListElement { text: "1 Maccabees 12"; value: "M112_0";}
        ListElement { text: "1 Maccabees 13"; value: "M113_0";}
        ListElement { text: "1 Maccabees 14"; value: "M114_0";}
        ListElement { text: "1 Maccabees 15"; value: "M115_0";}
        ListElement { text: "1 Maccabees 16"; value: "M116_0";}
    }
    ListModel {
        id: twomamod
        ListElement { text: "2 Maccabees 1"; value: "M21_0";}
        ListElement { text: "2 Maccabees 2"; value: "M22_0";}
        ListElement { text: "2 Maccabees 3"; value: "M23_0";}
        ListElement { text: "2 Maccabees 4"; value: "M24_0";}
        ListElement { text: "2 Maccabees 5"; value: "M25_0";}
        ListElement { text: "2 Maccabees 6"; value: "M26_0";}
        ListElement { text: "2 Maccabees 7"; value: "M27_0";}
        ListElement { text: "2 Maccabees 8"; value: "M28_0";}
        ListElement { text: "2 Maccabees 9"; value: "M29_0";}
        ListElement { text: "2 Maccabees 10"; value: "M210_0";}
        ListElement { text: "2 Maccabees 11"; value: "M211_0";}
        ListElement { text: "2 Maccabees 12"; value: "M212_0";}
        ListElement { text: "2 Maccabees 13"; value: "M213_0";}
        ListElement { text: "2 Maccabees 14"; value: "M214_0";}
        ListElement { text: "2 Maccabees 15"; value: "M215_0";}
    }
    ListModel {
        id: oneesmod
        ListElement { text: "1 Esdras 1"; value: "E11_0";}
        ListElement { text: "1 Esdras 2"; value: "E12_0";}
        ListElement { text: "1 Esdras 3"; value: "E13_0";}
        ListElement { text: "1 Esdras 4"; value: "E14_0";}
        ListElement { text: "1 Esdras 5"; value: "E15_0";}
        ListElement { text: "1 Esdras 6"; value: "E16_0";}
        ListElement { text: "1 Esdras 7"; value: "E17_0";}
        ListElement { text: "1 Esdras 8"; value: "E18_0";}
        ListElement { text: "1 Esdras 9"; value: "E19_0";}
    }
    ListModel {
        id: manmod
        ListElement { text: "Prayer of Manasses 1"; value: "PN1_0";}
    }
    ListModel {
        id: ps2mod
        ListElement { text: "Psalm 151 1"; value: "PX1_0";}
    }
    ListModel {
        id: threemamod
        ListElement { text: "3 Maccabees 1"; value: "M31_0";}
        ListElement { text: "3 Maccabees 2"; value: "M32_0";}
        ListElement { text: "3 Maccabees 3"; value: "M33_0";}
        ListElement { text: "3 Maccabees 4"; value: "M34_0";}
        ListElement { text: "3 Maccabees 5"; value: "M35_0";}
        ListElement { text: "3 Maccabees 6"; value: "M36_0";}
        ListElement { text: "3 Maccabees 7"; value: "M37_0";}
    }
    ListModel {
        id: twoesmod
        ListElement { text: "2 Esdras 1"; value: "E21_0";}
        ListElement { text: "2 Esdras 2"; value: "E22_0";}
        ListElement { text: "2 Esdras 3"; value: "E23_0";}
        ListElement { text: "2 Esdras 4"; value: "E24_0";}
        ListElement { text: "2 Esdras 5"; value: "E25_0";}
        ListElement { text: "2 Esdras 6"; value: "E26_0";}
        ListElement { text: "2 Esdras 7"; value: "E27_0";}
        ListElement { text: "2 Esdras 8"; value: "E28_0";}
        ListElement { text: "2 Esdras 9"; value: "E29_0";}
        ListElement { text: "2 Esdras 10"; value: "E210_0";}
        ListElement { text: "2 Esdras 11"; value: "E211_0";}
        ListElement { text: "2 Esdras 12"; value: "E212_0";}
        ListElement { text: "2 Esdras 13"; value: "E213_0";}
        ListElement { text: "2 Esdras 14"; value: "E214_0";}
        ListElement { text: "2 Esdras 15"; value: "E215_0";}
        ListElement { text: "2 Esdras 16"; value: "E216_0";}
    }
    ListModel {
        id: fourmamod
        ListElement { text: "4 Maccabees 1"; value: "M41_0";}
        ListElement { text: "4 Maccabees 2"; value: "M42_0";}
        ListElement { text: "4 Maccabees 3"; value: "M43_0";}
        ListElement { text: "4 Maccabees 4"; value: "M44_0";}
        ListElement { text: "4 Maccabees 5"; value: "M45_0";}
        ListElement { text: "4 Maccabees 6"; value: "M46_0";}
        ListElement { text: "4 Maccabees 7"; value: "M47_0";}
        ListElement { text: "4 Maccabees 8"; value: "M48_0";}
        ListElement { text: "4 Maccabees 9"; value: "M49_0";}
        ListElement { text: "4 Maccabees 10"; value: "M410_0";}
        ListElement { text: "4 Maccabees 11"; value: "M411_0";}
        ListElement { text: "4 Maccabees 12"; value: "M412_0";}
        ListElement { text: "4 Maccabees 13"; value: "M413_0";}
        ListElement { text: "4 Maccabees 14"; value: "M414_0";}
        ListElement { text: "4 Maccabees 15"; value: "M415_0";}
        ListElement { text: "4 Maccabees 16"; value: "M416_0";}
        ListElement { text: "4 Maccabees 17"; value: "M417_0";}
        ListElement { text: "4 Maccabees 18"; value: "M418_0";}
    }
    ListModel {
        id: dagmod
        ListElement { text: "Daniel (Greek) 1"; value: "DG1_0";}
        ListElement { text: "Daniel (Greek) 2"; value: "DG2_0";}
        ListElement { text: "Daniel (Greek) 3"; value: "DG3_0";}
        ListElement { text: "Daniel (Greek) 4"; value: "DG4_0";}
        ListElement { text: "Daniel (Greek) 5"; value: "DG5_0";}
        ListElement { text: "Daniel (Greek) 6"; value: "DG6_0";}
        ListElement { text: "Daniel (Greek) 7"; value: "DG7_0";}
        ListElement { text: "Daniel (Greek) 8"; value: "DG8_0";}
        ListElement { text: "Daniel (Greek) 9"; value: "DG9_0";}
        ListElement { text: "Daniel (Greek) 10"; value: "DG10_0";}
        ListElement { text: "Daniel (Greek) 11"; value: "DG11_0";}
        ListElement { text: "Daniel (Greek) 12"; value: "DG12_0";}
        ListElement { text: "Daniel (Greek) 13"; value: "DG13_0";}
        ListElement { text: "Daniel (Greek) 14"; value: "DG14_0";}
    }
}
